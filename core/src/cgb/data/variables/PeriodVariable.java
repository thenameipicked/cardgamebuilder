package cgb.data.variables;

import cgb.data.CardGame;
import cgb.data.time.Period;

public class PeriodVariable extends Variable<Period>{
    public PeriodVariable(CardGame game){
        super(game.startingPeriod);
    }
}
