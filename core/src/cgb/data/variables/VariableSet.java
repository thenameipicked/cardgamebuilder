package cgb.data.variables;

import cgb.data.Player;
import cgb.data.time.NeedsInitialization;

import java.util.*;

public class VariableSet implements NeedsInitialization{

    private final Map<String, Class<? extends Variable>> classes;
    private final Map<Class<? extends Variable>, Map<String, Variable>> containers;
    private final Set<String> notRemovable;
    private final Set<String> usedVariableNames;

    public VariableSet(){
        containers = new HashMap<Class<? extends Variable>, Map<String, Variable>>();
        notRemovable = new HashSet<String>();
        classes = new HashMap<String, Class<? extends Variable>>();
        usedVariableNames = new HashSet<String>();
        addVariable("Current Player", new PlayerVariable(), false);
    }

    @Override
    public void initialize(List<Player> players) {
        for (Map<String, Variable> container: containers.values()){
            for (Variable v: container.values()){
                v.initialize(players);
            }
        }
    }

    public void addVariable(String variableName, Variable variable){
        addVariable(variableName, variable, true);
    }

    public void addVariable(String variableName, Variable variable, boolean removable){
        if (usedVariableNames.contains(variableName)){
            throw new VariableExistsException();
        }
        usedVariableNames.add(variableName);
        Class<? extends Variable> t = variable.getClass();
        if (!containers.containsKey(t)){
            containers.put(t, new HashMap<String, Variable>());
        }
        classes.put(variableName, t);
        containers.get(t).put(variableName, variable);
        if (!removable){
            notRemovable.add(variableName);
        }
    }

    private void removeVariable(String variableName){
        if (notRemovable.contains(variableName)){
            throw new VariableNotRemovableException();
        }
        containers.get(classes.get(variableName)).remove(variableName);
    }

    public boolean hasVariable(String variableName){
        return classes.containsKey(variableName);
    }

    public Set<String> getVariableNames(){
        return new HashSet<String>(usedVariableNames);
    }

    public Set<String> getVariableNames(Class<?> variableType){
        return new HashSet<String>(containers.get(variableType).keySet());
    }

    public <T extends Variable> T getVariable(String variableName, Class<T> variableType){
        return variableType.cast(getVariable(variableName));
    }

    public Variable getVariable(String variableName){
        return containers.get(classes.get(variableName)).get(variableName);
    }

    public void setVariable(String variableName, Variable variable){
        containers.get(classes.get(variableName)).put(variableName, variable);
    }
}
