package cgb.data.variables;

import cgb.data.Player;
import cgb.data.time.NeedsInitialization;

import java.util.List;

public class PlayerVariable extends Variable<Player> implements NeedsInitialization {
    public PlayerVariable(){
        super(null);
    }

    @Override
    public void initialize(List<Player> players) {
        this.defaultValue = players.get(0);
        super.initialize(players);
    }
}
