package cgb.data.variables;

import cgb.data.Player;
import cgb.data.time.NeedsInitialization;

import java.util.List;

public abstract class Variable<T> implements NeedsInitialization{
    public T value;
    public T defaultValue;
    protected Variable(T defaultValue){
        value = null;
        this.defaultValue = defaultValue;
    }

    @Override
    public void initialize(List<Player> players) {
        this.value = defaultValue;
    }

}
