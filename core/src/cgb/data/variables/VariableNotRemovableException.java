package cgb.data.variables;

public class VariableNotRemovableException extends RuntimeException{
    public VariableNotRemovableException(){
        super("Variable is not removable");
    }
}
