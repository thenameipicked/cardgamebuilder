package cgb.data.variables;

public class VariableExistsException extends RuntimeException{
    public VariableExistsException(){
        super("Variable already exists");
    }
}
