package cgb.data;


import cgb.data.attributes.AttributeSet;
import cgb.data.attributes.HasAttributes;
import cgb.data.attributes.NumberRangeValidator;
import cgb.data.card.CardReserve;
import cgb.data.card.CardTable;
import cgb.data.time.NeedsInitialization;
import cgb.data.time.Period;
import cgb.data.variables.VariableSet;

import java.util.HashMap;
import java.util.List;


/**
 * Represents a card game.  Contains all of the parts necessary to build and play a card game.
 */
public class CardGame implements HasAttributes, NeedsInitialization{

    public CardReserve allCards;
    public Period startingPeriod;
    public HashMap<String, Period> allPeriods;
    public CardTable cardTable;
    public VariableSet variables;
    private AttributeSet attributes;
    public static final int MAX_PLAYERS = 32;
    public final static String MIN_PLAYERS_ATTRIBUTE = "Min players";
    public final static String MAX_PLAYERS_ATTRIBUTE = "Max players";
    private String fileName;

    public CardGame(){
        allPeriods = new HashMap<String, Period>();
        startingPeriod = new Period("Starting Period", this);
        allCards = new CardReserve();
        variables = new VariableSet();
        cardTable = new CardTable();
        attributes = new AttributeSet();
        attributes.newAttribute(MAX_PLAYERS_ATTRIBUTE, "0", false, new NumberRangeValidator(0, MAX_PLAYERS));
        attributes.newAttribute(MIN_PLAYERS_ATTRIBUTE, "1", false, new NumberRangeValidator(1, MAX_PLAYERS));
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
    public boolean hasFileName(){
        return fileName != null;
    }

    @Override
    public AttributeSet getAttributes() {
        return attributes;
    }

    @Override
    public void initialize(List<Player> players) {
        if (players.size() == 0)
            throw new NeedsPlayersException();
        cardTable.initialize(players);
        variables.initialize(players);
    }
}
