package cgb.data;

public class NeedsPlayersException extends RuntimeException{
    public NeedsPlayersException(){
        super("Needs players to start game");
    }
}
