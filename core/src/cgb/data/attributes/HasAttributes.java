package cgb.data.attributes;

public interface HasAttributes {
    public AttributeSet getAttributes();
}
