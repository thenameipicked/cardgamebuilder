package cgb.data.attributes;


/**
 * Attributes are the "settings" that can be used on cards, games, and other game objects.
 * The identifier is the type of setting, such as "Color", while the value is the actual setting
 * such as "Blue".  Intended to be stored in a HashMap<String, Attribute> where the key is the
 * identifier.
 */
public class Attribute {
    public final String identifier;
    public final String value;
    public Attribute(String identifier, String value){
        this.identifier = identifier;
        this.value = value;
    }

    @Override
    public int hashCode() {
        return 23^this.identifier.hashCode()^this.value.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null){
            return false;
        }
        if (obj instanceof Attribute) {
            Attribute attribute = (Attribute) obj;
            return attribute.identifier.equals(identifier) && attribute.value.equals(value);
        }
        return false;
    }
}
