package cgb.data.attributes;

/**
 * Ensures that the value is an integer
 */
public class NumberValidator implements AttributeValidator{
    private static NumberValidator validator;
    private NumberValidator(){}
    public static NumberValidator getValidator(){
        if (validator == null){
            validator = new NumberValidator();
        }
        return validator;
    }
    @Override
    public boolean isValid(String attributeValue) {
        try{
            Integer.parseInt(attributeValue);
            return true;
        }catch(NumberFormatException exception){
            return false;
        }
    }

    @Override
    public String errorMessage() {
        return "Must be a whole number";
    }
}
