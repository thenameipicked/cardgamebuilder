package cgb.data.attributes;

/**
 * Used to give attributes a "type", and ensure that their value fits certain parameters.
 *
 */
public interface AttributeValidator {
    /**
     * @param value is the value of the attribute
     * @return whether it is valid or not
     */
    public abstract boolean isValid(String value);

    /**
     * @return a user-friendly message as to what values are valid
     */
    public abstract String errorMessage();
}
