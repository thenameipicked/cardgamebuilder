package cgb.data.attributes;

import java.util.*;

public class AttributeSet {
    private final ArrayList<String> attributeNames;
    private final HashMap<String, Attribute> attributes;
    private final HashSet<String> finalAttributes;
    private final HashMap<String, AttributeValidator> validator;
    public AttributeSet(){
        attributes = new HashMap<String, Attribute>();
        finalAttributes = new HashSet<String>();
        validator = new HashMap<String, AttributeValidator>();
        attributeNames = new ArrayList<String>();
    }
    public AttributeSet(AttributeSet set){
        attributes = new HashMap<String, Attribute>(set.attributes);
        finalAttributes = new HashSet<String>(set.finalAttributes);
        validator = new HashMap<String, AttributeValidator>(set.validator);
        attributeNames = new ArrayList<String>(set.attributeNames);
    }
    public void newAttribute(String identifier, String value){
        newAttribute(identifier, value, true);
    }
    public void newAttribute(String identifier, String value, boolean removable){
        newAttribute(identifier, value, removable, DefaultValidator.getDefaultValidator());
    }
    public void newAttribute(String identifier, String value, boolean removable, AttributeValidator validator){
        Attribute attribute = new Attribute(identifier, value);
        attributes.put(attribute.identifier, attribute);
        attributeNames.add(attribute.identifier);
        if (!removable) {
            this.finalAttributes.add(attribute.identifier);
        }
        this.validator.put(attribute.identifier, validator);
    }
    public void setAttribute(String identifier, String value){
        AttributeValidator validator = this.validator.get(identifier);
        if (validator.isValid(value)) {
            attributes.put(identifier, new Attribute(identifier, value));
        } else {
            throw new InvalidAttributeException(validator);
        }

}
    public void removeAttribute(String identifier){
        if (!finalAttributes.contains(identifier)){
            attributes.remove(identifier);
            finalAttributes.remove(identifier);
            validator.remove(identifier);
            attributeNames.remove(identifier);
        } else {
            throw new InvalidAttributeException("You cannot remove this attribute");
        }
    }
    public Attribute getAttribute(String name){
        return attributes.get(name);
    }
    public boolean hasAttribute(String name){
        return attributes.containsKey(name);
    }
    public boolean hasAttribute(Attribute attribute){
        return hasAttribute(attribute.identifier) && attributes.get(attribute.identifier).equals(attribute);
    }
    public void setValidator(Attribute attribute, AttributeValidator validator){
        setValidator(attribute.identifier, validator);
    }
    public ArrayList<String> allAttributes(){
        return attributeNames;
    }

    public void setValidator(String name, AttributeValidator validator){
        if (finalAttributes.contains(name)){
            throw new InvalidAttributeException("You cannot change the type of attribute");
        } else {
            this.validator.put(name, validator);
        }
    }

    public void renameIdentifier(String previous, String next){
        if (finalAttributes.contains(previous)){
            throw new InvalidAttributeException("You cannot change the identifier");
        } else {
            attributes.put(next, new Attribute(next, attributes.remove(previous).value));
            validator.put(next, validator.remove(previous));
            attributeNames.set(attributeNames.indexOf(previous), next);
        }
    }

    public boolean isRemovable(Attribute attribute){
        return isRemovable(attribute.identifier);
    }

    public boolean isRemovable(String identifier){
        return !finalAttributes.contains(identifier);
    }

}
