package cgb.data.attributes;

public class BooleanValidator implements AttributeValidator{
    private static BooleanValidator validator;
    private BooleanValidator(){}

    public static BooleanValidator getValidator(){
        if (validator == null){
            validator = new BooleanValidator();
        }
        return validator;
    }
    @Override
    public boolean isValid(String value) {
        return value.equals("true") || value.equals("false");
    }

    @Override
    public String errorMessage() {
        return "Value must be 'true' or 'false'";
    }
}
