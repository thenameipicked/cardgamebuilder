package cgb.data.attributes;

/**
 * Ensures that the value is a integer between two numbers (both inclusive)
 */
public class NumberRangeValidator implements AttributeValidator{
    public final int minimum, maximum;
    public NumberRangeValidator(int minimum, int maximum){
        this.maximum = maximum;
        this.minimum = minimum;
    }

    @Override
    public boolean isValid(String attributeValue) {
        try{
            int value = Integer.parseInt(attributeValue);
            return value >= minimum && value <= maximum;
        } catch (NumberFormatException exception){
            return false;
        }
    }

    @Override
    public String errorMessage() {
        return "Must be a number from "+minimum+" to "+maximum;
    }
}
