package cgb.data.attributes;


/**
 * Allows any type of value
 */
public class DefaultValidator implements  AttributeValidator{
    private static DefaultValidator validator;

    private DefaultValidator(){}

    public static DefaultValidator getDefaultValidator(){
        if (validator == null){
            validator = new DefaultValidator();
        }
        return validator;
    }


    @Override
    public boolean isValid(String value) {
        return true;
    }

    @Override
    public String errorMessage() {
        return "";
    }
}
