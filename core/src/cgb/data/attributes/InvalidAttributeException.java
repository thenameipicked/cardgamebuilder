package cgb.data.attributes;


/**
 * This exception should be thrown if a value is assigned that doesn't pass validation
 */
public class InvalidAttributeException extends RuntimeException {
    public InvalidAttributeException(AttributeValidator validator){
        super(validator.errorMessage());
    }
    public InvalidAttributeException(String message){
        super(message);
    }
}
