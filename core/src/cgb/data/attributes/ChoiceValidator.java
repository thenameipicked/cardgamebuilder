package cgb.data.attributes;

import java.util.Collection;


/**
 * Ensures that that an attribute stores one out of a set of choices
 */
public class ChoiceValidator implements AttributeValidator{
    public final Collection<String> choices;
    public ChoiceValidator(Collection<String> choices){
        this.choices = choices;
    }

    @Override
    public boolean isValid(String value) {
        return choices.contains(value);
    }

    public String errorMessage(){
        StringBuilder message = new StringBuilder("Must be one of the following: ");
        for (String choice: choices){
            message.append(choice);
            message.append(", ");
        }
        return message.toString();
    }
}
