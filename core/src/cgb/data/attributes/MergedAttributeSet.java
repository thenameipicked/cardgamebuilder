package cgb.data.attributes;


import java.util.ArrayList;
import java.util.List;

public class MergedAttributeSet extends AttributeSet{
    private final List<AttributeSet> sets;
    public MergedAttributeSet(List<AttributeSet> sets){
        this.sets = sets;
    }

    @Override
    public void newAttribute(String identifier, String value, boolean removeable, AttributeValidator validator) {
        for (AttributeSet set: sets){
            set.newAttribute(identifier, value, removeable, validator);
        }
    }

    @Override
    public void removeAttribute(String identifier) {
        for (AttributeSet set: sets){
            set.removeAttribute(identifier);
        }
    }

    @Override
    public Attribute getAttribute(String name) {
        Attribute starting = sets.get(0).getAttribute(name);
        for (AttributeSet set: sets){
            if (!set.getAttribute(name).equals(starting)){
                return new Attribute(name, "...");
            }
        }
        return starting;
    }

    @Override
    public boolean hasAttribute(String attribute) {
        for (AttributeSet set: sets){
            if (!set.hasAttribute(attribute)){
                return false;
            }
        }
        return true;
    }

    @Override
    public void setValidator(String attribute, AttributeValidator validator) {
        for (AttributeSet set: sets){
            set.setValidator(attribute, validator);
        }
    }

    @Override
    public void setAttribute(String identifier, String value) {
        for (AttributeSet set: sets){
            set.setAttribute(identifier, value);
        }
    }

    @Override
    public ArrayList<String> allAttributes() {
        ArrayList<String> merged = new ArrayList<String>();
        if (sets.size() == 0){
            return merged;
        }
        merged.addAll(sets.get(0).allAttributes());
        for (AttributeSet set: sets){
            merged.retainAll(set.allAttributes());
        }
        return merged;
    }

    public void renameIdentifier(String previous, String next){
        if (!isRemovable(previous)){
            throw new InvalidAttributeException("You cannot change the identifier");
        }
        for (AttributeSet set:sets){
            set.renameIdentifier(previous, next);
        }
    }

    public boolean isRemovable(String identifier){
        for (AttributeSet set:sets){
            if (!set.isRemovable(identifier)){
                return false;
            }
        }
        return true;
    }
}
