package cgb.data.time.conditions;

import cgb.ui.screens.creategame.actors.actions.conditions.ConditionFactory;

import java.util.ArrayList;
import java.util.List;

public class OrCondition extends Condition{
    List<Condition> conditions;
    public OrCondition(){
        conditions = new ArrayList<>();
        conditions.add(new TrueCondition());
        conditions.add(new TrueCondition());
    }
    @Override
    public List<Condition> getDependentConditions() {
        return conditions;
    }


    @Override
    public boolean isTrue() {
        for (Condition condition: conditions){
            if (condition.isTrue()){
                return true;
            }
        }
        return false;
    }

    @Override
    public String getText() {
        return ConditionFactory.ConditionNames.OR;
    }
}
