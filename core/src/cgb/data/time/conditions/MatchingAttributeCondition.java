package cgb.data.time.conditions;

import cgb.data.attributes.HasAttributes;
import cgb.ui.screens.creategame.actors.actions.conditions.ConditionFactory;

import java.util.List;

public class MatchingAttributeCondition extends Condition{

    public HasAttributes object1, object2;
    public String identifier;

    @Override
    public boolean isTrue() {
        return object1.getAttributes().getAttribute(identifier).equals(object2.getAttributes().getAttribute(identifier));
    }

    @Override
    public List<Condition> getDependentConditions() {
        return null;
    }

    @Override
    public String getText() {
        return ConditionFactory.ConditionNames.MATCHING_ATTRIBUTES;
    }
}

