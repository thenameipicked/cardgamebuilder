package cgb.data.time.conditions;

import cgb.data.card.Deck;
import cgb.ui.screens.creategame.actors.actions.conditions.ConditionFactory;

import java.util.List;

public class DeckSizeCondition extends Condition{

    public boolean hasMin = false;
    public int minDeckSize = -1;
    public boolean hasMax = false;
    public int maxDeckSize = 100;
    public Deck deck;

    @Override
    public boolean isTrue(){
        return (hasMin && deck.getSize() >= minDeckSize) && (hasMax && deck.getSize() <= maxDeckSize);
    }

    @Override
    public List<Condition> getDependentConditions() {
        return null;
    }

    @Override
    public String getText() {
        return ConditionFactory.ConditionNames.DECK_SIZE;
    }
}
