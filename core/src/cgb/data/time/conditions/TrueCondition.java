package cgb.data.time.conditions;

import cgb.ui.screens.creategame.actors.actions.conditions.ConditionFactory;

import java.util.List;

public class TrueCondition extends Condition{

    public TrueCondition(){}

    @Override
    public boolean isTrue() {
        return true;
    }

    @Override
    public List<Condition> getDependentConditions() {
        return null;
    }

    @Override
    public String getText() {
        return ConditionFactory.ConditionNames.TRUE;
    }
}
