package cgb.data.time.conditions;

import cgb.ui.screens.creategame.actors.actions.conditions.ConditionFactory;

import java.util.ArrayList;
import java.util.List;

public class AndCondition extends Condition{
    List<Condition> conditions;
    public AndCondition(){
        conditions = new ArrayList<>();
        conditions.add(new TrueCondition());
        conditions.add(new TrueCondition());
    }
    @Override
    public List<Condition> getDependentConditions() {
        return conditions;
    }


    @Override
    public boolean isTrue() {
        for (Condition condition: conditions){
            if (!condition.isTrue()){
                return false;
            }
        }
        return true;
    }

    @Override
    public String getText() {
        return ConditionFactory.ConditionNames.AND;
    }
}
