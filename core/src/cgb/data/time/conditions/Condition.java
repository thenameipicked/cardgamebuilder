package cgb.data.time.conditions;

import java.util.List;

public abstract class Condition {
    public abstract boolean isTrue();
    public abstract List<Condition> getDependentConditions();
    public abstract String getText();
}
