package cgb.data.time.conditions;

import cgb.ui.screens.creategame.actors.actions.conditions.ConditionFactory;

import java.util.ArrayList;
import java.util.List;

public class NotCondition extends Condition{
    List<Condition> conditions;
    public NotCondition(){
        conditions = new ArrayList<>();
        conditions.add(new TrueCondition());
    }
    @Override
    public List<Condition> getDependentConditions() {
        return conditions;
    }


    @Override
    public boolean isTrue() {
        return !conditions.get(0).isTrue();
    }

    @Override
    public String getText() {
        return ConditionFactory.ConditionNames.NOT;
    }
}
