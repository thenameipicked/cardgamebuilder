package cgb.data.time;

import cgb.data.Player;

import java.util.List;

public interface NeedsInitialization {
    public void initialize(List<Player> players);
}
