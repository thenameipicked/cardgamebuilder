package cgb.data.time.actions;

public class ActionInput {
    private boolean needsInput;
    private String input;
    public ActionInput(boolean needsInput){
        this.needsInput = needsInput;
    }
    public ActionInput(boolean needsInput, String input){
        this.needsInput = needsInput;
        this.input = input;
    }
    public boolean needsInput(){
        return needsInput;
    }

    public void input(String string){
        this.input = string;
    }

    public String getInput() {
        return input;
    }

}
