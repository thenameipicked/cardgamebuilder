package cgb.data.time.actions;

import cgb.data.card.Card;
import cgb.data.card.Deck;
import cgb.ui.screens.creategame.actors.actions.ActionFactory;

import java.util.List;

public class MoveCardAction extends Action{
    public enum MoveOptions{
        Top, Bottom, Player_choice;

        @Override
        public String toString() {
            return super.toString().replace('_',' ');
        }
    }

    public Deck to;
    public Deck from;
    public MoveOptions toOption;
    public MoveOptions fromOption;
    private int fromPosition;
    private int toPosition;
    public int count;
    public MoveCardAction(){
        to = null;
        from = null;
        count = 1;
    }

    public void chooseToPosition(int toPosition){
        this.toPosition = toPosition;
    }

    public void chooseFromPosition(int fromPosition){
        this.fromPosition = fromPosition;
    }

    @Override
    public void act(ActionInput input) {
        List<Card> cards;
        int size = from.getSize();
        int amount = Math.min(count, size);
        switch(fromOption){
            case Bottom:
                cards = from.removeCards(size-amount, size);
                break;
            case Player_choice:
                int start = this.fromPosition;
                int stop = this.fromPosition+amount;
                if (stop > size){
                    start -= stop-size;
                    stop = size;
                }
                cards = from.removeCards(start, stop);
                break;
            case Top:
            default:
                cards = from.removeCards(0,amount);
                break;
        }
        switch (toOption){
            case Top:
                to.addCards(cards, 0);
                break;
            case Bottom:
                to.addCards(cards, to.getSize());
                break;
            case Player_choice:
                to.addCards(cards, toPosition);
                break;
        }
    }

    @Override
    public String getText() {
        return ActionFactory.ActionNames.MOVE;
    }
}
