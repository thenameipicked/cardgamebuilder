package cgb.data.time.actions;

import cgb.data.time.Period;
import cgb.ui.screens.creategame.actors.actions.ActionFactory;

import java.util.HashMap;

public class ChoiceAction extends Action{

    public final HashMap<String, Action> choices;
    private final ActionInput input;
    private final Period container;
    public ChoiceAction(Period container){
        choices = new HashMap<String, Action>();
        input = new ActionInput(true);
        this.container = container;
    }

    @Override
    public ActionInput getActionInput() {
        return input;
    }

    @Override
    public void act(ActionInput input) {
        container.moveToAction(choices.get(input.getInput()));
    }

    @Override
    public String getText() {
        return ActionFactory.ActionNames.CHOICE;
    }
}
