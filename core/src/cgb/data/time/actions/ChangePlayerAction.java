package cgb.data.time.actions;

import cgb.data.card.CardTable;
import cgb.ui.screens.creategame.actors.actions.ActionFactory;

public class ChangePlayerAction extends Action{

    private final CardTable cardTable;
    public ChangePlayerAction(CardTable cardTable){
        this.cardTable = cardTable;
    }

    @Override
    public void act(ActionInput input) {
        cardTable.nextPlayer();
    }

    @Override
    public String getText() {
        return ActionFactory.ActionNames.CHANGE_PLAYER;
    }
}
