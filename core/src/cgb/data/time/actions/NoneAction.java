package cgb.data.time.actions;

import cgb.ui.screens.creategame.actors.actions.ActionFactory;

public class NoneAction extends Action {

    public NoneAction() {}

    @Override
    public void act(ActionInput input) {}

    @Override
    public String getText() {
        return ActionFactory.ActionNames.NONE;
    }
}
