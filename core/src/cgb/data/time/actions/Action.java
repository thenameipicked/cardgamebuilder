package cgb.data.time.actions;

import cgb.data.attributes.AttributeSet;
import cgb.data.attributes.HasAttributes;
import com.thoughtworks.xstream.annotations.XStreamOmitField;


/**
 * A single action.
 */
public abstract class Action implements HasAttributes {
    private final AttributeSet attributes;
    @XStreamOmitField
    private final ActionInput input;
    public Action(){
        attributes = new AttributeSet();
        input = new ActionInput(false);
    }

    @Override
    public AttributeSet getAttributes() {
        return attributes;
    }

    public ActionInput getActionInput(){
        return input;
    }


    public abstract void act(ActionInput input);
    public abstract String getText();
}
