package cgb.data.time.actions;

import cgb.data.card.Card;
import cgb.data.card.CardTable;
import cgb.ui.screens.creategame.actors.actions.ActionFactory;

import java.util.ArrayList;

public class AddCardsAction extends Action{
    public ArrayList<Card> cards;
    public String addTo;
    public boolean toTop;
    private final CardTable table;
    public AddCardsAction(CardTable table){
        cards = new ArrayList<Card>();
        this.addTo = null;
        this.toTop = true;
        this.table = table;
    }


    @Override
    public void act(ActionInput input) {
        table.getDeck(addTo).addCards(cards, toTop?cards.size():0);

    }

    @Override
    public String getText() {
        return ActionFactory.ActionNames.ADD_CARDS;
    }
}
