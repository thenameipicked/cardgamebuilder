package cgb.data.time.actions;

import cgb.data.card.Deck;
import cgb.ui.screens.creategame.actors.actions.ActionFactory;

public class ShuffleAction extends Action{
    private Deck deck;
    public void setDeck(Deck deck){
        this.deck = deck;
    }
    public ShuffleAction(){
        deck = null;
    }
    @Override
    public void act(ActionInput input) {
        deck.shuffle();
    }


    @Override
    public String getText() {
        return ActionFactory.ActionNames.SHUFFLE;
    }
}
