package cgb.data.time.actions;

import cgb.data.time.Period;
import cgb.ui.screens.creategame.actors.actions.ActionFactory;

public class PeriodAction extends Action{


    public Period periodToPerform;
    private final Period host;

    public void setPeriodToPerform(Period periodToPerform) {
        this.periodToPerform = periodToPerform;
    }

    public PeriodAction(Period host){
        super();
        periodToPerform = null;
        this.host = host;
    }

    @Override
    public void act(ActionInput input) {
        host.moveToPeriod(periodToPerform);
    }

    @Override
    public String getText() {
        return ActionFactory.ActionNames.PERIOD;
    }
}
