package cgb.data.time.actions;

import cgb.data.time.Period;
import cgb.data.time.conditions.Condition;
import cgb.data.time.conditions.TrueCondition;
import cgb.ui.screens.creategame.actors.actions.ActionFactory;

public class ConditionalAction extends Action {
    public Condition condition;
    public Action trueAction;
    public Action falseAction;
    private final Period parent;
    public ConditionalAction(Period parent){
        this.condition = new TrueCondition();
        this.trueAction = new NoneAction();
        this.falseAction = new NoneAction();
        this.parent = parent;
    }

    @Override
    public void act(ActionInput input) {
        if (condition.isTrue()){
            parent.moveToAction(trueAction);
        } else {
            parent.moveToAction(falseAction);
        }
    }

    @Override
    public String getText() {
        return ActionFactory.ActionNames.CONDITION;
    }
}
