package cgb.data.time.actions;

import cgb.data.variables.VariableSet;
import cgb.ui.screens.creategame.actors.actions.ActionFactory;

public class SetVariableAction extends Action{
    public String from, to;
    private final VariableSet set;
    public SetVariableAction(VariableSet set){
        this.from = null;
        this.to = null;
        this.set = set;
    }

    @Override
    public void act(ActionInput action) {
        this.set.setVariable(to, this.set.getVariable(from));
    }

    @Override
    public String getText() {
        return ActionFactory.ActionNames.SET_VARIABLE;
    }
}
