package cgb.data.time;

import cgb.data.CardGame;
import cgb.data.time.actions.Action;
import cgb.data.time.actions.ActionInput;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents a section of game time.
 * Contains a list of sequential actions
 */
public class Period{
    public final List<Action> actions;
    private int currentAction;
    private Period currentPeriod;
    private String name;
    private final CardGame parent;
    public Period(String name, CardGame parent){
        actions = new ArrayList<Action>();
        currentAction = 0;
        currentPeriod = this;
        this.parent = parent;
        this.name = name;
        parent.allPeriods.put(name, this);
    }

    public String getName() {
        return name;
    }

    public void rename(String newName){
        if (!parent.allPeriods.containsKey(newName)) {
            parent.allPeriods.put(newName, parent.allPeriods.remove(name));
            this.name = newName;
        }
    }
    public void act(ActionInput input){
        if (currentPeriod == this) {
            actions.get(currentAction++).act(input);
        } else {
            currentPeriod.act(input);
        }
    }
    public void moveToAction(Action action){
        currentAction = actions.indexOf(action);
    }
    public void moveToPeriod(Period period){
        this.currentPeriod = period;
    }
    public void restart(){
        currentAction = 0;
    }
    public boolean isFinished(){
        return (currentAction == actions.size());
    }


}

