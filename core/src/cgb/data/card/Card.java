package cgb.data.card;

import cgb.data.attributes.*;

public class Card implements HasAttributes {
    private final AttributeSet attributes;
    private String title;
    private String description;

    public Card() {
        attributes = new AttributeSet();
        title = "";
        description = "";
    }
    public Card(Card card){
        attributes = new AttributeSet(card.attributes);
        title = card.title;
        description = card.description;
    }

    @Override
    public AttributeSet getAttributes() {
        return attributes;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public String getTitle() {
        return title;
    }
}
