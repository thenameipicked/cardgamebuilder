package cgb.data.card;

/**
 * Should be thrown if a card is not allowed in deck
 */
public class CardNotAllowedException extends RuntimeException{
    public CardNotAllowedException(String string){
        super(string);
    }
}
