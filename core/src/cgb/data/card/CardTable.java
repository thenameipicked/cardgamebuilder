package cgb.data.card;

import cgb.data.Player;
import cgb.data.time.NeedsInitialization;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Organizes all of the game objects
 * Structures the players, their cards, and other player-specific game data
 * Also contains global decks and items
 */
public class CardTable implements NeedsInitialization{
    private final HashMap<String, Deck> globalDecks;
    private final HashMap<String, Deck> playerDeckPrototypes;
    public List<Player> players;
    public HashMap<Player, HashMap<String, Deck>> playerDecks;
    private int currentPlayer;
    private boolean initialized;
    public CardTable(){
        globalDecks = new HashMap<String, Deck>();
        playerDeckPrototypes = new HashMap<String, Deck>();
        initialized = false;
    }

    public Deck getDeck(String deckName){
        if (globalDecks.containsKey(deckName)){
            return globalDecks.get(deckName);
        } else {
            if (initialized){
                return playerDecks.get(getCurrentPlayer()).get(deckName);
            } else {
                return playerDeckPrototypes.get(deckName);
            }
        }
    }

    public void removeDeck(Deck deck){
        (isGlobalDeck(deck)?globalDecks:playerDeckPrototypes).remove(deck.getAttributes().getAttribute(Deck.DECK_NAME_ATTRIBUTE).value);
    }

    public boolean isGlobalDeck(Deck deck){
        return isGlobalDeck(deck.getAttributes().getAttribute(Deck.DECK_NAME_ATTRIBUTE).value);
    }
    public boolean isGlobalDeck(String name){
        return globalDecks.containsKey(name);
    }

    public void renameDeck(String oldName, String newName){
        HashMap<String, Deck> decks = globalDecks.containsKey(oldName)?globalDecks:playerDeckPrototypes;
        Deck deck = decks.remove(oldName);
        deck.getAttributes().setAttribute(Deck.DECK_NAME_ATTRIBUTE, newName);
        playerDeckPrototypes.put(newName, deck);
    }

    public Deck newDeck(boolean global){
        String name = "Deck";
        int index = 1;
        while (globalDecks.containsKey(name) || playerDeckPrototypes.containsKey(name)){
            name = "Deck"+index++;
        }
        Deck deck = new Deck(name);
        (global ? globalDecks:playerDeckPrototypes).put(name, deck);
        return deck;
    }

    public void addDeck(Deck deck, boolean global){
        String name = deck.getAttributes().getAttribute(Deck.DECK_NAME_ATTRIBUTE).value;
        if (global){
            globalDecks.put(name, deck);
        } else {
            playerDeckPrototypes.put(name, deck);
        }
    }

    public boolean deckExists(String deckName){
        return globalDecks.containsKey(deckName) || playerDeckPrototypes.containsKey(deckName);
    }

    public Set<String> getDeckNames(){
        HashSet<String> names = new HashSet<String>(playerDeckPrototypes.keySet());
        names.addAll(globalDecks.keySet());
        return names;
    }

    public void nextPlayer(){
        currentPlayer = (currentPlayer+1)%playerDecks.size();
    }


    public Player getCurrentPlayer(){
        return players.get(currentPlayer);
    }

    /**
     * Should be called when a game is started
     */
    public void initialize(List<Player> players){
        playerDecks = new HashMap<Player, HashMap<String, Deck>>(players.size());
        this.players = players;
        for (Player player: players){
            HashMap<String, Deck> newDecks = new HashMap<String, Deck>(playerDeckPrototypes);
            playerDecks.put(player, newDecks);
        }
        initialized = true;
    }
}
