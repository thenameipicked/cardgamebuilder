package cgb.data.card;

import cgb.data.attributes.Attribute;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * Contains all of the Cards in the game
 */
public class CardReserve {
    private final List<Card> cards;
    public CardReserve(){
        cards = new ArrayList<Card>();
    }
    public List<Card> getCards(){
        return cards;
    }
    public List<Card> getCards(Set<Attribute> filters){
        ArrayList<Card> filtered = new ArrayList<Card>();
        for (Card card: cards){
            boolean shouldAdd = true;
            for (Attribute attribute: filters){
                if (!card.getAttributes().hasAttribute(attribute)) {
                    shouldAdd = false;
                    break;
                }
            }
            if (shouldAdd){
                filtered.add(card);
            }
        }
        return filtered;
    }
    public Card addCard(){
        Card card = new Card();
        cards.add(card);
        return card;
    }
    public void removeCard(Card card){
        cards.remove(card);
    }
    public void removeCards(Collection<Card> cards){
        this.cards.removeAll(cards);
    }


    /**
     * Creates cards with "Number" attributes from start to finish (inclusive)
     */
    public List<Card> addNumberedCards(int start, int finish){
        List<Card> addedCards = new ArrayList<Card>(finish-start+1);
        for (int i = start; i <= finish; i++){
            Card cardToAdd = new Card();
            cardToAdd.getAttributes().newAttribute("Number",""+i);
            addedCards.add(cardToAdd);
            cards.add(cardToAdd);
        }
        return addedCards;
    }

    /**
     * Takes some cards, and makes exact duplicates of them, returning the duplicates
     */
    public List<Card> duplicate(List<Card> cards){
        List<Card> duplicated = new ArrayList<Card>();
        for (Card card: cards){
            Card newCard = new Card(card);
            duplicated.add(newCard);
        }
        for (Card card: duplicated){
            this.cards.add(card);
        }
        return duplicated;
    }
    public void addAttribute(List<Card> cards, Attribute attribute){
        for (Card card: cards){
            if (card.getAttributes().hasAttribute(attribute)){
                card.getAttributes().setAttribute(attribute.identifier, attribute.value);
            } else {
                card.getAttributes().newAttribute(attribute.identifier, attribute.value);
            }
        }
    }
}
