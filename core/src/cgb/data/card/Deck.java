package cgb.data.card;

import cgb.data.attributes.*;

import java.util.*;

public class Deck implements HasAttributes {
    private final ArrayList<Card> deck;
    private final AttributeSet attributes;
    public final static String MIN_DECK_SIZE_ATTRIBUTE = "Max size";
    public final static String MAX_DECK_SIZE_ATTRIBUTE = "Min size";
    public final static String DECK_FILTER_ATTRIBUTE = "Attributes required";
    public final static String DECK_NAME_ATTRIBUTE = "Name";
    public final static String DECK_VIEW_ATTRIBUTE = "Public view";

    public final static String DECK_VIEW_FACE_DOWN_PILE = "Face-down pile";
    public final static String DECK_VIEW_FACE_DOWN_PILE_COUNT = "Face-down pile + count";
    public final static String DECK_VIEW_FACE_UP_PILE = "Face-up pile";
    public final static String DECK_VIEW_FACE_UP_PILE_COUNT = "Face-up pile + count";
    public final static String DECK_VIEW_SPREAD = "Spread";

    private final static int MAX_DECK_SIZE = 10000;

    public Deck(String name){
        attributes = new AttributeSet();
        attributes.newAttribute(MAX_DECK_SIZE_ATTRIBUTE, "-1", false, new NumberRangeValidator(-1, MAX_DECK_SIZE));
        attributes.newAttribute(MIN_DECK_SIZE_ATTRIBUTE, "0", false, new NumberRangeValidator(-1, MAX_DECK_SIZE));
        attributes.newAttribute(DECK_FILTER_ATTRIBUTE, "None", false);
        attributes.newAttribute(DECK_NAME_ATTRIBUTE, name, false);
        Set<String> views = new HashSet<String>();
        views.add(DECK_VIEW_FACE_DOWN_PILE);
        views.add(DECK_VIEW_FACE_DOWN_PILE_COUNT);
        views.add(DECK_VIEW_FACE_UP_PILE);
        views.add(DECK_VIEW_FACE_UP_PILE_COUNT);
        views.add(DECK_VIEW_SPREAD);
        attributes.newAttribute(DECK_VIEW_ATTRIBUTE, DECK_VIEW_FACE_DOWN_PILE, false, new ChoiceValidator(views));
        deck = new ArrayList<Card>();
    }

    @Override
    public AttributeSet getAttributes() {
        return attributes;
    }

    /**
     * Creates a copy of the deck with the same attributes, but no cards
     */
    public Deck(Deck deck){
        attributes = new AttributeSet(deck.attributes);
        this.deck = new ArrayList<Card>();
    }

    public void addCard(Card card, int position){
        int maxSize = Integer.parseInt(attributes.getAttribute(MAX_DECK_SIZE_ATTRIBUTE).value);
        if (maxSize != 0 && maxSize >= deck.size()){
            throw new CardNotAllowedException("Deck is full");
        }
        Attribute allowsCards = attributes.getAttribute(DECK_FILTER_ATTRIBUTE);
        if (!allowsCards.value.equals("None")){
            for (String requiredAttribute: allowsCards.value.split(",")){
                if (!card.getAttributes().hasAttribute(requiredAttribute.trim())){
                    throw new CardNotAllowedException("Card must have '"+requiredAttribute.trim()+"' attribute");
                }
            }
        }
        deck.add(position, card);
    }
    public void addCards(List<Card> cards, int position){
        ArrayList<Card> previous = new ArrayList<Card>(this.deck);
        try {
            for (int i = 0; i < cards.size(); i++) {
                addCard(cards.get(i), position + i);
            }
        } catch (CardNotAllowedException e){
            this.deck.clear();
            this.deck.addAll(previous);
            throw e;
        }
    }

    public void shuffle(){
        Collections.shuffle(deck);
    }

    public Card removeCard(int position){
        return deck.remove(position);
    }
    public List<Card> removeCards(int start, int stop){
        List<Card> list = new ArrayList<>(deck.subList(start, stop));
        deck.removeAll(list);
        return list;
    }

    public int getSize(){
        return deck.size();
    }
}
