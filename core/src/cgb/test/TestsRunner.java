package cgb.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;


@RunWith(Suite.class)
@Suite.SuiteClasses({
    UnoCreator.class,
    AttributeTester.class
})
public class TestsRunner {

}
