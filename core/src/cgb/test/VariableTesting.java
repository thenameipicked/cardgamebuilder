package cgb.test;

import cgb.data.CardGame;
import cgb.data.Player;
import cgb.data.variables.NumberVariable;
import cgb.data.variables.PeriodVariable;
import cgb.data.variables.PlayerVariable;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class VariableTesting {

    @Test
    public void testVariables(){
        CardGame game = new CardGame();

        PlayerVariable p1 = new PlayerVariable();
        assertEquals(p1.defaultValue, null);
        assertEquals(p1.value, null);

        PeriodVariable p2 = new PeriodVariable(game);
        assertEquals(p2.defaultValue, game.startingPeriod);
        assertEquals(p2.value, null);

        NumberVariable p3 = new NumberVariable();
        assertEquals(p3.defaultValue, (Integer)0);
        assertEquals(p3.value, null);


    }

    @Test
    public void testVariableInit(){
        CardGame game = new CardGame();
        Player p = new Player();
        List<Player> players = new ArrayList<Player>();
        players.add(p);
        game.variables.addVariable("var1", new PlayerVariable());
        game.initialize(players);
        assertEquals(game.variables.getVariable("var1").defaultValue,p);
    }

}

