package cgb.test;

import cgb.data.attributes.AttributeSet;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AttributeTester {
    @Test
    public void attributesCanBeStored(){
        AttributeSet set = new AttributeSet();
        assertEquals(set.allAttributes().size(), 0);
        set.newAttribute("attr1", "1");
        assertEquals(set.allAttributes().size(), 1);
        set.newAttribute("attr2", "2");
        assertEquals(set.allAttributes().size(), 2);
        assertEquals(set.getAttribute("attr1").value, "1");
        assertEquals(set.getAttribute("attr2").value, "2");
        assertEquals(set.getAttribute("attr1").identifier, "attr1");
        assertEquals(set.getAttribute("attr2").identifier, "attr2");
        set.removeAttribute("attr1");
        assertEquals(set.getAttribute("attr1"), null);
        assertEquals(set.getAttribute("attr2").value, "2");
        set.removeAttribute("attr1");
        assertEquals(set.allAttributes().size(), 1);
        set.removeAttribute("attr1");
        set.removeAttribute("attr2");
        assertEquals(set.allAttributes().size(), 0);
    }
}
