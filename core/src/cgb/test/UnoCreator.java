package cgb.test;

import cgb.data.CardGame;
import cgb.data.attributes.Attribute;
import cgb.data.card.Card;
import cgb.data.card.Deck;
import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;

/**
 * Creates a playable uno game
 */
public class UnoCreator {

    CardGame uno;

    @Before
    public void createGame(){
        //Create Cards
        uno = new CardGame();
        List<Card> cards = uno.allCards.addNumberedCards(1, 10);
        uno.allCards.addAttribute(cards, new Attribute("Color", "Yellow"));
        List<Card> duplicated = uno.allCards.duplicate(cards);
        uno.allCards.addAttribute(duplicated, new Attribute("Color", "Green"));
        duplicated = uno.allCards.duplicate(cards);
        uno.allCards.addAttribute(duplicated, new Attribute("Color", "Blue"));
        duplicated = uno.allCards.duplicate(cards);
        uno.allCards.addAttribute(duplicated, new Attribute("Color", "Red"));
        //Create Decks


        Deck discard = uno.cardTable.newDeck(true);
        uno.cardTable.renameDeck(discard.getAttributes().getAttribute(Deck.DECK_NAME_ATTRIBUTE).value, "Discard");

        Deck draw = uno.cardTable.newDeck(true);
        uno.cardTable.renameDeck(draw.getAttributes().getAttribute(Deck.DECK_NAME_ATTRIBUTE).value, "Draw");

        Deck hand = uno.cardTable.newDeck(true);
        uno.cardTable.renameDeck(hand.getAttributes().getAttribute(Deck.DECK_NAME_ATTRIBUTE).value, "Hand");

    }

    @Test
    public void filterCards() {
        assertEquals(uno.allCards.getCards().size(),40);//Make 40 cards exist
        Attribute color = new Attribute("Color", "Red");
        Set<Attribute> filter = new HashSet<Attribute>();
        filter.add(color);
        assertEquals(uno.allCards.getCards(filter).size(),10);//Make sure only 10 of each color exists
        Attribute number = new Attribute("Number", "1");
        filter.add(number);
        assertEquals(uno.allCards.getCards(filter).size(),1);//Make sure only 1 of each number/color exists
        filter.remove(color);
        assertEquals(uno.allCards.getCards(filter).size(),4);//Make sure 4 of each number exists
    }


}
