package cgb.network;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Net;
import com.badlogic.gdx.net.Socket;

import java.io.InputStream;
import java.io.OutputStream;

public class Client extends Connection{
    Socket socket = null;
    public void connect() {
        try {
            socket = Gdx.net.newClientSocket(Net.Protocol.TCP, "127.0.0.1", 31415, null); // protocol, ip, port
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public String getiStream(){
        InputStream iStream = socket.getInputStream();
        System.out.print(iStream); // For debugging
        return iStream.toString();
    }
    public void parseInput(String input) {

        // Parse input string to get formatted turn information
        // struct? turnInfo?
        // return turnInfo or doTurn
    }
    public void loadGame()
    {
        // Manipulate input string to get formatted game information: rules, cards, etc.
        // struct? turnInfo?
        // return turnInfo

    }
    public void generatePacket()
    {
        String packet = "test packet";
        sendTurn(packet);
    }

    public void sendTurn(String packet){
        OutputStream oStream;
        try
        {
            oStream = socket.getOutputStream();
                    oStream.write(packet.getBytes());
            oStream.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
