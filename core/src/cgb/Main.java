package cgb;

import cgb.data.CardGame;
import cgb.ui.screens.StartScreen;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;



public class Main extends Game {
	StartScreen screen;
	Skin skin;
	CardGame game;

	@Override
	public void create () {
		Gdx.graphics.setTitle("Card Game Creator");
        skin = new Skin(Gdx.files.internal("core/assets/data/uiskin.json"));
		game = new CardGame();
		screen = new StartScreen(this, skin, game);

		setScreen(screen);
	}

	public void refreshGame(CardGame game){
		screen = new StartScreen(this, skin, game);
		this.game = game;
		setScreen(screen);
	}

    @Override
	public void dispose() {
		super.dispose();
		screen.dispose();
        skin.dispose();
	}
}
