package cgb.io;

import cgb.data.CardGame;
import cgb.data.attributes.*;
import cgb.data.card.Card;
import cgb.data.card.CardReserve;
import cgb.data.card.CardTable;
import cgb.data.card.Deck;
import cgb.data.time.Period;
import cgb.data.time.actions.*;
import cgb.data.time.conditions.*;
import cgb.data.variables.*;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.thoughtworks.xstream.XStream;

public class SaveGame {
    private final static FileHandle SAVED_GAMES_DIRECTORY =
            (Gdx.files.isExternalStorageAvailable()?Gdx.files.external("cgb"):Gdx.files.internal("cgb")).child("saved");

    private final static XStream xstream = new XStream();
    static {
        xstream.alias("game", CardGame.class);
        xstream.alias("reserve", CardReserve.class);
        xstream.alias("card", Card.class);
        xstream.alias("table", CardTable.class);
        xstream.alias("deck", Deck.class);
        xstream.alias("period", Period.class);
        xstream.alias("variables", VariableSet.class);
        xstream.alias("attributes", AttributeSet.class);
        xstream.alias("attribute", Attribute.class);

        xstream.alias("action", Action.class);
        xstream.alias("action.addcards", AddCardsAction.class);
        xstream.alias("action.changeplayer", ChangePlayerAction.class);
        xstream.alias("action.choice", ChoiceAction.class);
        xstream.alias("action.conditional", ConditionalAction.class);
        xstream.alias("action.movecard", MoveCardAction.class);
        xstream.alias("action.none", NoneAction.class);
        xstream.alias("action.period", PeriodAction.class);
        xstream.alias("action.setvariable", SetVariableAction.class);
        xstream.alias("action.shuffle", ShuffleAction.class);

        xstream.alias("condition", Condition.class);
        xstream.alias("condition.decksize", DeckSizeCondition.class);
        xstream.alias("condition.matchingattribute", MatchingAttributeCondition.class);
        xstream.alias("condition.truecondition", TrueCondition.class);

        xstream.alias("variable", Variable.class);
        xstream.alias("variable.number", NumberVariable.class);
        xstream.alias("variable.period", PeriodVariable.class);
        xstream.alias("variable.player", PlayerVariable.class);


        xstream.alias("validator.boolean", BooleanValidator.class);
        xstream.alias("validator.choice", ChoiceValidator.class);
        xstream.alias("validator.default", DefaultValidator.class);
        xstream.alias("validator.numberrange", NumberRangeValidator.class);
        xstream.alias("validator.number", NumberValidator.class);

    }
    private static String gameToXml(CardGame game){
        return xstream.toXML(game);
    }
    private static CardGame xmlToGame(String xml){
        return (CardGame) xstream.fromXML(xml);
    }
    public static void saveGame(CardGame game, String name){
        FileHandle handle = SAVED_GAMES_DIRECTORY.child(name+".cgb");
        handle.writeString(gameToXml(game), false);
    }
    public static String[] getGameNames(){
        FileHandle[] files = SAVED_GAMES_DIRECTORY.list();
        String[] names = new String[files.length];
        for (int i = 0; i < names.length; i++){
            names[i] = files[i].name();
            if (names[i].contains(".cgb")){
                names[i] = names[i].substring(0, names[i].indexOf(".cgb"));
            }
        }
        return names;
    }
    public static CardGame getGame(String name){
        name = name + ".cgb";
        String xml = SAVED_GAMES_DIRECTORY.child(name).readString();
        return xmlToGame(xml);
    }
    public void deleteGame(String name){
        SAVED_GAMES_DIRECTORY.child(name).delete();
    }
}
