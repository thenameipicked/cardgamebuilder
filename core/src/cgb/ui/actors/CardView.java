package cgb.ui.actors;

import cgb.data.attributes.Attribute;
import cgb.data.card.Card;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

import java.util.ArrayList;
import java.util.List;


public class CardView extends Actor {
    private final Card card;
    private final Table table;
    private final Label title;
    private final Label description;
    private final List<AttributeTextField> attributes;

    public static final int MIN_CARD_WIDTH = 40;
    public static final int MAX_CARD_WIDTH = MIN_CARD_WIDTH * 5;
    public static final float CARD_HEIGHT_TO_WIDTH_RATIO = 1.4f;

    public CardView(Card card, Skin skin){
        super();
        this.card = card;
        this.table = new Table();
        this.title = new Label(card.getTitle(), skin);
        this.description = new Label(card.getDescription(), skin);
        this.attributes = new ArrayList<AttributeTextField>();
        table.add(title);
        table.row();
        for (String identifier: card.getAttributes().allAttributes()){
            attributes.add(new AttributeTextField(card.getAttributes().getAttribute(identifier), skin));
            table.row();
        }
        table.add(this.description);
    }
    private final class AttributeTextField extends Label {
        public AttributeTextField(Attribute attribute, Skin skin){
            super(attribute.identifier+": "+attribute.value, skin);
        }
    }

    @Override
    public void act(float delta) {
        super.act(delta);

    }

    @Override
    protected void sizeChanged() {
        title.setWidth(getWidth());
        title.invalidate();

        table.setWidth(getWidth());
        table.setHeight(getHeight());


    }

    @Override
    protected void positionChanged() {
        table.setPosition(getX(), getY());
    }
}
