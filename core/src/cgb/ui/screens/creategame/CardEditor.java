package cgb.ui.screens.creategame;

import cgb.data.attributes.AttributeSet;
import cgb.data.attributes.MergedAttributeSet;
import cgb.data.card.Card;
import cgb.data.card.CardReserve;
import cgb.ui.screens.MenuScreen;
import cgb.ui.screens.creategame.actors.CardGui;
import cgb.ui.screens.creategame.actors.EditableCard;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Ben on 4/8/15.
 * MainMenu -> Create Game -> Deck Editor
 */
public class CardEditor extends MenuScreen {
    //Static variables
    public static final int CARDS_PER_ROW = 8, PADDING = 5;
    private static final float SIDE_BAR_WIDTH = 150f;
    //Member variables
    private int lastCardClicked = 0;
    private final List<CardGui> displayedCards;
    private final Set<CardGui> selectedCards;
    //Actors
    private final Table cards, sideBar;
    private final ScrollPane cardPane;
    private final TextButton addCard, duplicate, remove, editAttributes, back;
    private final EditableCard editableCard;


    public CardEditor(final Game parent, final CardReserve reserve, final Skin skin, final Screen origin){
        super(parent, skin);

        selectedCards = new HashSet<CardGui>();
        displayedCards = new ArrayList<CardGui>(reserve.getCards().size());
        for (Card card: reserve.getCards()){
            newCard(card, skin);
        }

        cards = new Table();
        cardPane = new ScrollPane(cards);
        addCard = new TextButton("New Card", skin);
        remove = new TextButton("Remove Selected", skin);
        duplicate = new TextButton("Duplicate Selected", skin);
        editAttributes = new TextButton("Edit Attributes", skin);
        back = new TextButton("Back", skin);
        sideBar = new Table();
        editableCard = new EditableCard(selectedCards, skin);


        editAttributes.setColor(Color.DARK_GRAY);
        duplicate.setColor(Color.DARK_GRAY);
        remove.setColor(Color.DARK_GRAY);


        stage.addActor(cardPane);
        stage.addActor(sideBar);




        addCard.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                newCard(reserve.addCard(), skin);
                layout();
            }
        });
        remove.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                ArrayList<CardGui> notRemoved = new ArrayList<CardGui>();
                for (CardGui gui : new ArrayList<CardGui>(displayedCards)) {
                    if (!gui.isSelected()) {
                        notRemoved.add(gui);
                    } else {
                        reserve.removeCard(gui.getCard());
                    }
                }
                selectedCards.clear();
                displayedCards.clear();
                displayedCards.addAll(notRemoved);
                layout();
                editableCard.update();
            }
        });
        duplicate.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                for (CardGui gui: new ArrayList<CardGui>(displayedCards)){
                    if (gui.isSelected()) {
                        newCard(new Card(gui.getCard()), skin);
                    }
                }
                layout();
            }
        });
        back.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                parent.setScreen(origin);
            }
        });

        editableCard.setTextFieldListener(new TextField.TextFieldListener() {
            @Override
            public void keyTyped(TextField textField, char c) {
                for (CardGui gui: displayedCards){
                    gui.update();
                }
            }
        });
        editAttributes.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                ArrayList<AttributeSet> sets = new ArrayList<AttributeSet>();
                if (selectedCards.size() == 0){
                    return;
                }
                for (CardGui displayed: selectedCards){
                    sets.add(displayed.getCard().getAttributes());
                }
                AttributeEditor editor = new AttributeEditor(parent, new MergedAttributeSet(sets), skin, CardEditor.this);
                parent.setScreen(editor);
            }
        });
    }

    public void newCard(Card card, Skin skin){
        final CardGui cardGui = new CardGui(card, skin);
        cardGui.setClickListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                int nextClicked = displayedCards.indexOf(cardGui);
                if (Gdx.input.isKeyPressed(Input.Keys.SHIFT_LEFT)){
                    if (nextClicked < lastCardClicked){
                        int swap = nextClicked;
                        nextClicked = lastCardClicked;
                        lastCardClicked = swap;
                        displayedCards.get(nextClicked).click();
                    }
                    for (int i = lastCardClicked; i < nextClicked; i++){
                        CardGui card = displayedCards.get(i);
                        card.selected();
                        selectedCards.add(card);
                    }
                } else if (!Gdx.input.isKeyPressed(Input.Keys.CONTROL_LEFT)){
                    for (CardGui gui: selectedCards){
                        gui.notSelected();
                    }
                    selectedCards.clear();
                }
                CardGui nextCard = displayedCards.get(nextClicked);
                nextCard.click();
                if (nextCard.isSelected()) {
                    selectedCards.add(nextCard);
                } else {
                    selectedCards.remove(nextCard);
                }
                lastCardClicked = nextClicked;
                editableCard.update();
                if (selectedCards.size() == 0) {
                    editAttributes.setColor(Color.DARK_GRAY);
                    duplicate.setColor(Color.DARK_GRAY);
                    remove.setColor(Color.DARK_GRAY);
                } else {
                    editAttributes.setColor(Color.WHITE);
                    duplicate.setColor(Color.WHITE);
                    remove.setColor(Color.WHITE);
                }
            }
        });
        displayedCards.add(cardGui);
    }

    @Override
    public void render(float delta) {
        super.render(delta);
    }

    @Override
    public void show() {
        super.show();
        for (CardGui displayed: displayedCards) {
            displayed.update();
        }
        editableCard.update();
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
        layout();
    }
    private void layout(){
        float height = stage.getHeight();
        float width = stage.getWidth();
        sideBar.clear();
        sideBar.defaults().pad(PADDING).width(SIDE_BAR_WIDTH - PADDING * 2);
        sideBar.add(addCard).row();
        sideBar.add(editAttributes).row();
        sideBar.add(duplicate).row();
        sideBar.add(remove).row();
        sideBar.add(editableCard).height((SIDE_BAR_WIDTH - PADDING * 2) * 1.5f).row();
        sideBar.add(back).row();
        sideBar.setPosition(width - SIDE_BAR_WIDTH, 0);
        sideBar.setHeight(height);
        sideBar.setWidth(SIDE_BAR_WIDTH);
        float remainingWidth = width - SIDE_BAR_WIDTH;
        cardPane.setPosition(0, 0);
        cardPane.setHeight(height);
        cardPane.setWidth(remainingWidth);
        cards.clear();

        float cardWidth = remainingWidth/ CARDS_PER_ROW - PADDING *2;
        cards.defaults().width(cardWidth).height(cardWidth*1.5f).pad(PADDING);
        for (int i = 0; i < displayedCards.size(); i++){
            cards.add(displayedCards.get(i));
            if (i% CARDS_PER_ROW == CARDS_PER_ROW -1){
                cards.row();
            }
        }
    }
}
