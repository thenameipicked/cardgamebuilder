package cgb.ui.screens.creategame;

import cgb.data.CardGame;
import cgb.ui.screens.MenuScreen;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;

public class CreateGameScreen extends MenuScreen {

    final CardGame game;
    final Game gameParent;

    public CreateGameScreen(Game parent, Skin skin, Screen origin, CardGame game) {
        super(parent, skin);

        gameParent = parent;
        this.game = game;

        Button editCards = new TextButton("Card Editor", skin);
        addActor(editCards, new CardEditor(parent, game.allCards, skin, this));
        Button editTable = new TextButton("Game CardTable Editor", skin);
        addActor(editTable, new TableEditor(parent, skin, game, this));
        Button editTimeline = new TextButton("Timeline Editor", skin);
        addActor(editTimeline, new TimelineEditor(parent, skin, game, this));
        Button editButton = new TextButton("Edit Game Settings", skin);
        addActor(editButton, new AttributeEditor(parent, game.getAttributes(), skin, this));
        Button back = new TextButton("Main Menu", skin);
        addActor(back, origin);
    }

}
