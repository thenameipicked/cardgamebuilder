package cgb.ui.screens.creategame;

import cgb.data.CardGame;
import cgb.data.time.Period;
import cgb.data.time.actions.Action;
import cgb.data.time.actions.NoneAction;
import cgb.ui.screens.MenuScreen;
import cgb.ui.screens.creategame.actors.PeriodGui;
import cgb.ui.screens.creategame.actors.actions.ActionFactory;
import cgb.ui.screens.creategame.actors.actions.ActionGui;
import cgb.ui.screens.creategame.actors.actions.ActionGuiFactory;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import java.util.ArrayList;

public class TimelineEditor extends MenuScreen {
    public static final float SETTINGS_WIDTH = 200f, PADDING = 10f;
    Table settings;
    ScrollPane timelinePane;
    Table timelineTable;
    ArrayList<PeriodGui> periods;
    CardGame game;
    TextButton back, insertBefore, insertAfter, removeAction, removePeriod, addPeriod;
    PeriodGui selectedPeriod;
    ActionGui selectedAction;
    SelectBox<String> options;
    Skin skin;

    public TimelineEditor(final Game parent, final Skin skin, final CardGame game, final Screen origin){
        super(parent, skin);
        periods = new ArrayList<PeriodGui>();
        this.skin = skin;
        for (Period period: game.allPeriods.values()){
            PeriodGui periodGui = createPeriod(period);
            for (Action action: period.actions){
                ActionGui actionGui = createAction(action);
                periodGui.actions.add(actionGui);
            }
            periods.add(periodGui);
        }
        options = new SelectBox<String>(skin);
        options.getSelection().setProgrammaticChangeEvents(false);
        options.setItems(ActionFactory.ActionNames.allActionNames.toArray(new String[ActionFactory.ActionNames.allActionNames.size()]));


        this.game = game;

        back = new TextButton("Back", skin);
        insertBefore = new TextButton("Insert Action Before", skin);
        insertAfter = new TextButton("Insert Action After", skin);
        removeAction = new TextButton("Remove Action", skin);
        addPeriod = new TextButton("Add Period", skin);
        removePeriod = new TextButton("Remove Period", skin);

        settings = new Table();
        timelineTable = new Table();
        timelinePane = new ScrollPane(timelineTable);


        back.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                parent.setScreen(origin);
            }
        });
        insertBefore.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                if (selectedPeriod != null){
                    if (selectedAction == null){
                        ActionGui action = createAction(new NoneAction());
                        selectedPeriod.getPeriod().actions.add(0, action.getAction());
                        selectedPeriod.actions.add(0, action);
                    } else {
                        int index = selectedPeriod.actions.indexOf(selectedAction);
                        ActionGui action = createAction(new NoneAction());
                        selectedPeriod.getPeriod().actions.add(index, action.getAction());
                        selectedPeriod.actions.add(index, action);
                    }
                    layout();
                }
            }
        });
        insertAfter.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                if (selectedPeriod != null){
                    if (selectedAction == null){
                        ActionGui action = createAction(new NoneAction());
                        selectedPeriod.getPeriod().actions.add(action.getAction());
                        selectedPeriod.actions.add(action);
                    } else {
                        ActionGui action = createAction(new NoneAction());
                        int index = selectedPeriod.actions.indexOf(selectedAction)+1;
                        selectedPeriod.getPeriod().actions.add(index, action.getAction());
                        selectedPeriod.actions.add(index, createAction(new NoneAction()));
                    }
                    layout();
                }
            }
        });
        removeAction.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                if (selectedAction != null){
                    selectedPeriod.getPeriod().actions.remove(selectedAction.getAction());
                    selectedPeriod.actions.remove(selectedAction);
                    selectedAction = null;
                    layout();
                }
            }
        });
        removePeriod.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                if (selectedPeriod != null) {
                    TimelineEditor.this.game.allPeriods.remove(selectedPeriod.getPeriod().getName());
                    periods.remove(selectedPeriod);
                    selectedPeriod = null;
                    selectedAction = null;
                    layout();
                }
            }
        });
        addPeriod.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                String name = "name";
                int counter = 1;
                while (game.allPeriods.containsKey(name)){
                    name = "name"+counter++;
                }
                createPeriod(new Period(name, game));
                layout();
            }
        });
        options.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                if (selectedAction != null) {
                    Action action = ActionFactory.createAction(options.getSelected(), game, selectedPeriod.getPeriod());
                    ActionGui gui = createAction(action);
                    int index = selectedPeriod.getPeriod().actions.indexOf(selectedAction.getAction());
                    selectedPeriod.getPeriod().actions.set(index, action);
                    selectedPeriod.actions.set(index, gui);
                    selectedAction = gui;
                    selectedAction.select();
                    layout();
                }
            }
        });

        stage.addActor(settings);
        stage.addActor(timelinePane);
    }

    private ActionGui createAction(Action action){
        final ActionGui actionGui = ActionGuiFactory.createGui(action, skin, game, selectedPeriod.getPeriod());
        final PeriodGui periodGui = selectedPeriod;
        actionGui.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                if (selectedAction != null) {
                    selectedAction.deselect();
                }
                if (selectedPeriod != null) {
                    selectedPeriod.deselect();
                }
                selectedAction = actionGui;
                selectedPeriod = periodGui;
                selectedAction.select();
                selectedPeriod.select();
                options.setSelected(selectedAction.getText().toString());
                layoutSettings();
            }
        });
        return actionGui;
    }
    private PeriodGui createPeriod(Period period){
        final PeriodGui periodGui = new PeriodGui(period, skin);
        periods.add(periodGui);
        periodGui.title.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                if (selectedPeriod != null) {
                    selectedPeriod.deselect();
                }
                if (selectedAction != null) {
                    selectedAction.deselect();
                }
                selectedPeriod = periodGui;
                selectedAction = null;
                selectedPeriod.select();
                layoutSettings();
            }
        });
        return periodGui;
    }

    @Override
    public void show() {
        super.show();
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
        layout();
    }
    public void layoutSettings(){
        settings.clear();
        settings.setPosition(0, 0);
        settings.setWidth(SETTINGS_WIDTH);
        settings.setHeight(stage.getHeight());
        settings.defaults().width(SETTINGS_WIDTH - PADDING).padBottom(PADDING);
        settings.add(back).row();
        settings.add(insertBefore).row();
        settings.add(insertAfter).row();
        settings.add(addPeriod).row();
        settings.add(removeAction).row();
        settings.add(removePeriod).row();
        settings.add(options).row();
        settings.add().fillY().expandY().row();
        if (selectedAction != null && selectedAction.getOptions() != null){
            for (Actor actor: selectedAction.getOptions()){
                settings.add(actor).row();
            }
        }


        if (selectedPeriod == null){
            insertAfter.setColor(Color.DARK_GRAY);
            insertBefore.setColor(Color.DARK_GRAY);
        } else {
            insertAfter.setColor(Color.WHITE);
            insertBefore.setColor(Color.WHITE);
        }
        if (selectedAction == null){
            removeAction.setColor(Color.DARK_GRAY);
            options.setColor(Color.DARK_GRAY);
        } else {
            removeAction.setColor(Color.WHITE);
            options.setColor(Color.WHITE);
        }

    }
    public void layout(){
        float width = stage.getWidth();
        float height = stage.getHeight();
        float remainingWidth = width-SETTINGS_WIDTH;

        layoutSettings();

        timelinePane.setPosition(SETTINGS_WIDTH, 0);
        timelinePane.setWidth(remainingWidth);
        timelinePane.setHeight(height);
        timelinePane.setScrollingDisabled(false, true);

        timelineTable.clear();
        timelineTable.defaults().height(height).pad(PADDING / 2);
        for (PeriodGui periodGui: periods){
            timelineTable.add(periodGui);
            periodGui.relayout();
        }

    }
}
