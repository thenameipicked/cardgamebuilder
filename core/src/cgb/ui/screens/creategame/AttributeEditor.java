package cgb.ui.screens.creategame;

import cgb.data.attributes.Attribute;
import cgb.data.attributes.AttributeSet;
import cgb.data.attributes.InvalidAttributeException;
import cgb.ui.screens.MenuScreen;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import java.util.ArrayList;

public class AttributeEditor extends MenuScreen {

    AttributeSet set;
    Table table;
    Game gameParent;
    ArrayList <Table> attributeList;
    Button back, newAttribute;
    Skin skin;

    public AttributeEditor(final Game parent, AttributeSet set, Skin skin, Screen origin){
        super(parent, skin);
        this.set = set;
        this.skin = skin;
        attributeList = new ArrayList<Table>();
        table = new Table();
        back = new TextButton("Back", skin);
        newAttribute = new TextButton("Add new Attribute", skin);

        newAttribute.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                AttributeEditor.this.set.newAttribute("name", "value");
                relayout();
            }
        });

        addActor(table);
        addActor(newAttribute);
        addActor(back, origin);
        relayout();
    }

    @Override
    public void show() {
        super.show();
    }

    public void relayout(){
        table.clear();
        for (final String attribute : set.allAttributes()){
            AttributeTable table = new AttributeTable(attribute, skin);
            AttributeEditor.this.table.add(table).row();
        }
    }

    private class AttributeTable extends Table {
        private TextField identifier, value;
        private String previousName;
        private Button remove;
        public AttributeTable(String identifier, Skin skin){
            Attribute attribute = set.getAttribute(identifier);
            setSkin(skin);
            this.identifier = new TextField(attribute.identifier, skin);
            this.value = new TextField(attribute.value, skin);
            this.remove = new TextButton("Remove", skin);
            this.identifier.setColor(Color.LIGHT_GRAY);
            this.value.setColor(Color.LIGHT_GRAY);
            this.previousName = identifier;
            this.identifier.setTextFieldListener(new TextField.TextFieldListener() {
                @Override
                public void keyTyped(TextField textField, char c) {
                    try {
                        set.renameIdentifier(previousName, textField.getText());
                        AttributeTable.this.previousName = textField.getText();
                        textField.setColor(Color.LIGHT_GRAY);
                    } catch (InvalidAttributeException exception){
                        textField.setColor(Color.RED);
                    }
                }
            });
            this.value.setTextFieldListener(new TextField.TextFieldListener() {
                @Override
                public void keyTyped(TextField field, char character) {
                    try {
                        set.setAttribute(previousName, field.getText());
                        setColor(Color.LIGHT_GRAY);
                    } catch (InvalidAttributeException e){
                        setColor(Color.RED);
                    }
                }
            });
            this.remove.addListener(new ClickListener(){
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    super.clicked(event, x, y);
                    set.removeAttribute(previousName);
                    relayout();
                }
            });

            defaults().pad(5).width(150);
            if (set.isRemovable(attribute)) {
                add(this.identifier);
            } else {
                add(identifier+":");
            }
            add(value);
            if (set.isRemovable(attribute)) {
                add(remove);
            } else {
                add();
            }
        }
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
        table.setPosition(width/2, height/2);
    }
}
