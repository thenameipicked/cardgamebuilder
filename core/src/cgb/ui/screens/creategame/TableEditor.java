package cgb.ui.screens.creategame;

import cgb.data.CardGame;
import cgb.data.card.CardTable;
import cgb.data.card.Deck;
import cgb.ui.actors.CardView;
import cgb.ui.screens.MenuScreen;
import cgb.ui.screens.creategame.actors.DeckGui;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ben on 4/8/15.
 * MainMenu -> Create Game -> Deck Editor
 */
public class TableEditor extends MenuScreen {
    public static final float CARD_WIDTH = 100f;
    CardTable table;
    Table attributes;
    Table globalDecks;
    ScrollPane playerDecksPane;
    Table playerDecks;
    List<DeckGui> global, player;
    Skin skin;
    TextButton back, newGlobalDeck, newPlayerDeck, editAttributes, removeDeck;
    DeckGui selectedDeck;


    public TableEditor(final Game parent, final Skin skin, CardGame game, final Screen origin){
        super(parent, skin);
        table = game.cardTable;
        attributes = new Table(skin);
        globalDecks = new Table(skin);
        playerDecks = new Table(skin);
        playerDecksPane = new ScrollPane(playerDecks);
        playerDecksPane.setScrollingDisabled(false, true);
        back = new TextButton("Back", skin);
        newGlobalDeck = new TextButton("New Global Deck", skin);
        newPlayerDeck = new TextButton("New Player Deck", skin);
        editAttributes = new TextButton("Edit Deck Attributes", skin);
        removeDeck = new TextButton("Remove Deck", skin);

        newPlayerDeck.setColor(Color.LIGHT_GRAY);
        newGlobalDeck.setColor(Color.LIGHT_GRAY);


        global = new ArrayList<DeckGui>();
        player = new ArrayList<DeckGui>();

        back.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                parent.setScreen(origin);
            }
        });
        newGlobalDeck.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                newDeck(table.newDeck(true), true);
                relayout();
            }
        });
        newPlayerDeck.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                newDeck(table.newDeck(false), false);
                relayout();
            }
        });
        editAttributes.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                if (selectedDeck != null){
                    AttributeEditor editor = new AttributeEditor(parent, selectedDeck.getDeck().getAttributes(),skin, TableEditor.this);
                    parent.setScreen(editor);
                }
            }
        });
        removeDeck.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                if (selectedDeck != null){
                    (table.isGlobalDeck(selectedDeck.getDeck())?global:player).remove(selectedDeck);
                    table.removeDeck(selectedDeck.getDeck());
                    selectedDeck = null;
                    relayout();
                }
            }
        });
        this.skin = skin;

        stage.addActor(attributes);
        stage.addActor(globalDecks);
        stage.addActor(playerDecksPane);

    }

    public DeckGui newDeck(Deck deck, boolean globalDeck){
        final DeckGui gui = new DeckGui(deck, skin, table);
        gui.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (selectedDeck != null){
                    selectedDeck.clicked();
                }
                selectedDeck = gui;
                selectedDeck.clicked();
                editAttributes.setColor(Color.LIGHT_GRAY);
                removeDeck.setColor(Color.LIGHT_GRAY);
            }
        });
        (globalDeck?global:player).add(gui);
        return gui;
    }

    @Override
    public void show() {
        super.show();
    }
    public void relayout(){
        float width = stage.getWidth();
        float height = stage.getHeight();



        attributes.clear();
        attributes.setPosition(0, 0);
        attributes.setWidth(width / 4);
        attributes.setHeight(height);
        attributes.defaults().width(width / 4 - 10).pad(5);
        attributes.add(newGlobalDeck).row();
        attributes.add(newPlayerDeck).row();
        attributes.add(removeDeck).row();
        attributes.add(editAttributes).row();
        attributes.add().expandY().fillY().row();
        attributes.add(back).row();
        playerDecksPane.setWidth(3 * width / 4);
        playerDecksPane.setHeight(CARD_WIDTH * CardView.CARD_HEIGHT_TO_WIDTH_RATIO);
        playerDecksPane.setPosition(width, 0, Align.bottomRight);
        globalDecks.setPosition(width, height, Align.topRight);
        globalDecks.setWidth(3 * width / 4);
        globalDecks.setHeight(height-playerDecksPane.getHeight());

        int cardsPerRow = (int)(globalDecks.getWidth()/(CARD_WIDTH+10f));
        globalDecks.clear();
        playerDecks.clear();
        globalDecks.defaults().width(CARD_WIDTH).height(CardView.CARD_HEIGHT_TO_WIDTH_RATIO*CARD_WIDTH).pad(5);
        playerDecks.defaults().width(CARD_WIDTH).height(CardView.CARD_HEIGHT_TO_WIDTH_RATIO*CARD_WIDTH).pad(5);
        for (int i = 0; i < global.size(); i++) {
            globalDecks.add(global.get(i));
            if (i%cardsPerRow == cardsPerRow-1) {
                globalDecks.row();
            }
        }
        for (DeckGui gui: player){
            playerDecks.add(gui);
        }
        if (selectedDeck == null) {
            editAttributes.setColor(Color.DARK_GRAY);
            removeDeck.setColor(Color.DARK_GRAY);
        } else {
            editAttributes.setColor(Color.LIGHT_GRAY);
            removeDeck.setColor(Color.LIGHT_GRAY);
        }
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
        relayout();
    }
}
