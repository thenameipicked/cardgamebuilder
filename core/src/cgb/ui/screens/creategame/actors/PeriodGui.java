package cgb.ui.screens.creategame.actors;

import cgb.data.time.Period;
import cgb.ui.screens.creategame.actors.actions.ActionGui;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import java.util.ArrayList;
import java.util.List;

public class PeriodGui extends Table {
    public static final int PERIOD_WIDTH = 200, PADDING = 5;
    public final List<ActionGui> actions;
    public final TextField title;
    private Table actionTable;
    private ScrollPane scrollPane;
    private Period period;
    private ClickListener clickListener, titleClicked;

    public PeriodGui(final Period period, Skin skin){

        actions = new ArrayList<ActionGui>();
        this.period = period;
        clickListener = null;

        title = new TextField(period.getName(), skin);
        actionTable = new Table();
        scrollPane = new ScrollPane(actionTable);

        add(title);
        add(scrollPane);

        title.setTextFieldListener(new TextField.TextFieldListener() {
            @Override
            public void keyTyped(TextField textField, char c) {
                period.rename(title.getText());
            }
        });
    }


    public void select(){
        title.setColor(Color.PINK);
    }
    public void deselect(){
        title.setColor(Color.WHITE);
    }
    public Period getPeriod() {
        return period;
    }

    public void relayout(){
        clear();
        setWidth(PERIOD_WIDTH);
        defaults().width(PERIOD_WIDTH);
        add(title).row();
        add(scrollPane);
        actionTable.clear();
        actionTable.setWidth(PERIOD_WIDTH);
        actionTable.defaults().width(PERIOD_WIDTH-PADDING*2).pad(PADDING).padBottom(0);
        for (ActionGui actionGui: actions){
            actionTable.add(actionGui).row();
        }
    }
}
