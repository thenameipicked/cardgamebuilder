package cgb.ui.screens.creategame.actors;

import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextArea;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;

import java.util.Set;

public class EditableCard extends Table {

    TextField title;
    TextField description;
    Set<CardGui> cards;
    TextField.TextFieldListener listener;
    Skin skin;
    public EditableCard(Set<CardGui> cards, Skin skin){
        this.cards = cards;
        title = new TextField("", skin);
        description = new TextArea("", skin);
        this.skin = skin;
        update();
        addListeners();
    }
    public void setTextFieldListener(TextField.TextFieldListener listener) {
        this.listener = listener;
    }

    private void addListeners(){
        title.setTextFieldListener(new TextField.TextFieldListener() {
            @Override
            public void keyTyped(TextField textField, char c) {
                String text = title.getText();
                for (CardGui card : cards) {
                    card.getCard().setTitle(text);
                }
                if (listener != null){
                    listener.keyTyped(textField, c);
                }
            }
        });
        description.setTextFieldListener(new TextField.TextFieldListener() {
            @Override
            public void keyTyped(TextField textField, char c) {
                String text = description.getText();
                for (CardGui card : cards) {
                    card.getCard().setDescription(text);
                }
                if (listener != null){
                    listener.keyTyped(textField, c);
                }
            }
        });
    }
    public void update(){
        if (cards.size() == 0){
            title.setText("No Card Selected");
            description.setText("No Card Selected");
            return;
        }
        CardGui first = cards.iterator().next();
        String commonTitle = first.getCard().getTitle();
        String commonDescription = first.getCard().getDescription();
        for (CardGui card: cards){
            if (!card.getCard().getTitle().equals(commonTitle)){
                commonTitle = "...";
                break;
            }
        }
        for (CardGui card: cards){
            if (!card.getCard().getDescription().equals(commonDescription)){
                commonDescription = "...";
                break;
            }
        }
        title.setText(commonTitle);
        description.setText(commonDescription);
        title.setCursorPosition(commonTitle.length());

    }

    @Override
    protected void sizeChanged() {
        this.clear();
        this.add(title).height(getHeight()/4f).width(getWidth());
        this.row();
        this.add(description).height(getHeight()*3/4f).width(getWidth());
    }

}
