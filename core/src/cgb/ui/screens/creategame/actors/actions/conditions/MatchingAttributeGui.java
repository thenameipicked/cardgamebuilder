package cgb.ui.screens.creategame.actors.actions.conditions;

import cgb.data.time.conditions.MatchingAttributeCondition;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

public class MatchingAttributeGui extends ConditionGui{
    MatchingAttributeCondition condition;
    public MatchingAttributeGui(Skin skin, MatchingAttributeCondition condition){
        super(skin);
        this.condition = condition;
    }

    @Override
    public MatchingAttributeCondition getCondition() {
        return condition;
    }
}
