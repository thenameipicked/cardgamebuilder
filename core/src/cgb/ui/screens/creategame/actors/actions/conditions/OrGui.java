package cgb.ui.screens.creategame.actors.actions.conditions;

import cgb.data.time.conditions.OrCondition;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

public class OrGui extends ConditionGui{
    OrCondition condition;
    public OrGui(Skin skin, OrCondition condition){
        super(skin);
        this.condition = condition;
    }

    @Override
    public OrCondition getCondition() {
        return condition;
    }
}
