package cgb.ui.screens.creategame.actors.actions.conditions;

import cgb.data.time.conditions.NotCondition;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

public class NotGui extends ConditionGui{
    NotCondition condition;
    public NotGui(Skin skin, NotCondition condition){
        super(skin);
        this.condition = condition;
    }

    @Override
    public NotCondition getCondition() {
        return condition;
    }
}
