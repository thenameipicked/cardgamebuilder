package cgb.ui.screens.creategame.actors.actions;

import cgb.data.CardGame;
import cgb.data.time.Period;
import cgb.data.time.actions.*;

import java.util.ArrayList;
import java.util.List;

public class ActionFactory {
    public static final class ActionNames{
        public static final String ADD_CARDS = "Add Cards to Deck";
        public static final String CHANGE_PLAYER = "Change Player";
        public static final String CHOICE = "Present Choice";
        public static final String CONDITION = "If";
        public static final String MOVE = "Move Card";
        public static final String NONE = "Do Nothing";
        public static final String PERIOD = "Perform Period";
        public static final String SET_VARIABLE = "Set Variable";
        public static final String SHUFFLE = "Shuffle Deck";
        public static final List<String> allActionNames = new ArrayList<String>();
        static {
            allActionNames.add(ADD_CARDS);
            allActionNames.add(CHANGE_PLAYER);
            allActionNames.add(CHOICE);
            allActionNames.add(CONDITION);
            allActionNames.add(MOVE);
            allActionNames.add(NONE);
            allActionNames.add(PERIOD);
            allActionNames.add(SET_VARIABLE);
            allActionNames.add(SHUFFLE);
        }
    }
    public static Action createAction(String actionName, CardGame game, Period currentPeriod){
        if (actionName.equals(ActionNames.ADD_CARDS)){
            return new AddCardsAction(game.cardTable);
        } else if (actionName.equals(ActionNames.CHANGE_PLAYER)){
            return new ChangePlayerAction(game.cardTable);
        } else if (actionName.equals(ActionNames.CHOICE)){
            return new ChoiceAction(currentPeriod);
        } else if (actionName.equals(ActionNames.CONDITION)){
            return new ConditionalAction(currentPeriod);
        } else if (actionName.equals(ActionNames.MOVE)){
            return new MoveCardAction();
        } else if (actionName.equals(ActionNames.NONE)){
            return new NoneAction();
        } else if (actionName.equals(ActionNames.PERIOD)){
            return new PeriodAction(currentPeriod);
        } else if (actionName.equals(ActionNames.SET_VARIABLE)){
            return new SetVariableAction(game.variables);
        } else if (actionName.equals(ActionNames.SHUFFLE)){
            return new ShuffleAction();
        }
        return null;
    }
}
