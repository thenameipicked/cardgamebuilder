package cgb.ui.screens.creategame.actors.actions;

import cgb.data.CardGame;
import cgb.data.card.Card;
import cgb.data.time.actions.AddCardsAction;
import cgb.ui.screens.creategame.actors.CardGui;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class AddCards extends ActionGui{
    public static final float WINDOW_WIDTH = 800f, WINDOW_HEIGHT = 600f;
    public static final int CARDS_PER_ROW = 8, PADDING = 5;
    public static final float CARD_WIDTH = WINDOW_WIDTH/CARDS_PER_ROW;
    public static final float CARD_HEIGHT = CARD_WIDTH*1.5f;
    private final AddCardsAction action;
    private final SelectBox<String> decks;
    private final Label decksLabel;
    private final TextButton pickCards;
    private final Dialog cardPicker;
    private final CheckBox addToBottom;
    private final ArrayList<Actor> optionsList;

    private boolean pickingCards;
    public AddCards(Skin skin, AddCardsAction action, CardGame game){
        super(ActionFactory.ActionNames.ADD_CARDS, skin);
        this.action = action;

        decksLabel = new Label("Add cards to:", skin);
        decks = new SelectBox<String>(skin);
        pickCards = new TextButton("Select Cards", skin);
        addToBottom = new CheckBox("Add to Bottom", skin);


        cardPicker = new Dialog("Select Cards", skin);
        cardPicker.setWidth(WINDOW_WIDTH);
        cardPicker.setHeight(WINDOW_HEIGHT);
        cardPicker.setPosition(getWidth()/2, getHeight()/2);
        cardPicker.getContentTable().defaults().height(CARD_HEIGHT).width(CARD_WIDTH).pad(PADDING);
        List<Card> cards = game.allCards.getCards();
        for (int i = 0; i < cards.size(); i++){
            final CardGui gui = new CardGui(cards.get(i), skin);
            gui.setClickListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    super.clicked(event, x, y);
                    gui.click();
                    if (gui.isSelected()) {
                        AddCards.this.action.cards.add(gui.getCard());
                    } else {
                        AddCards.this.action.cards.remove(gui.getCard());
                    }
                }
            });
            cardPicker.getContentTable().add(gui);
            if (i%CARDS_PER_ROW == CARDS_PER_ROW-1){
                cardPicker.getContentTable().row();
            }
        }
        TextButton finished = new TextButton("Finished", skin);
        finished.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                AddCards.this.cardPicker.remove();
            }
        });
        cardPicker.button(finished);



        Set<String> deckNames = game.cardTable.getDeckNames();
        decks.getItems().addAll(deckNames.toArray(new String[deckNames.size()]));
        if (deckNames.isEmpty()){
            decks.setSelected("Empty");
        } else {
            decks.setSelectedIndex(0);
        }
        decks.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                AddCards.this.action.addTo = decks.getSelected();
            }
        });
        pickCards.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                getStage().addActor(cardPicker);
            }
        });
        addToBottom.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                AddCards.this.action.toTop = !addToBottom.isChecked();
            }
        });


        optionsList = new ArrayList<Actor>();
        optionsList.add(decksLabel);
        optionsList.add(decks);
        optionsList.add(pickCards);
        optionsList.add(addToBottom);



    }
    @Override
    public List<Actor> getOptions() {
        return optionsList;
    }

    @Override
    public AddCardsAction getAction() {
        return action;
    }
}
