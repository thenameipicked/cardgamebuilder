package cgb.ui.screens.creategame.actors.actions.conditions;

import cgb.data.time.conditions.Condition;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

public abstract class ConditionGui extends Table {
    public ConditionGui(Skin skin){
        setSkin(skin);
    }

    public abstract Condition getCondition();
}
