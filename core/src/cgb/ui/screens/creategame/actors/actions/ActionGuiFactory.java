package cgb.ui.screens.creategame.actors.actions;

import cgb.data.CardGame;
import cgb.data.time.Period;
import cgb.data.time.actions.*;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

public class ActionGuiFactory {
    public static ActionGui createGui(Action from, Skin skin, CardGame game, Period currentPeriod){
        if (from.getText().equals(ActionFactory.ActionNames.ADD_CARDS)){
            return new AddCards(skin, (AddCardsAction) from, game);
        } else if (from.getText().equals(ActionFactory.ActionNames.CHANGE_PLAYER)){
            return new ChangePlayer(skin, (ChangePlayerAction) from);
        } else if (from.getText().equals(ActionFactory.ActionNames.CHOICE)){
            return new Choice(skin, (ChoiceAction)from, currentPeriod);
        } else if (from.getText().equals(ActionFactory.ActionNames.CONDITION)){
            return new Conditional(skin, (ConditionalAction)from);
        } else if (from.getText().equals(ActionFactory.ActionNames.MOVE)){
            return new MoveCard(skin,(MoveCardAction) from, game.cardTable);
        } else if (from.getText().equals(ActionFactory.ActionNames.NONE)){
            return new None(skin, (NoneAction)from);
        } else if (from.getText().equals(ActionFactory.ActionNames.PERIOD)){
            return new SwitchPeriod(skin, (PeriodAction) from);
        } else if (from.getText().equals(ActionFactory.ActionNames.SET_VARIABLE)){
            return new SetVariable(skin, (SetVariableAction) from);
        } else if (from.getText().equals(ActionFactory.ActionNames.SHUFFLE)){
            return new Shuffle(skin, (ShuffleAction) from);
        }
        return new None(skin, new NoneAction());
    }
}
