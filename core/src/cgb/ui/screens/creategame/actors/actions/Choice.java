package cgb.ui.screens.creategame.actors.actions;

import cgb.data.time.Period;
import cgb.data.time.actions.Action;
import cgb.data.time.actions.ChoiceAction;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import java.util.ArrayList;
import java.util.List;

public class Choice extends ActionGui{
    private final ChoiceAction action;
    private final List<Actor> actors;
    private final Dialog choiceDialog;
    private final Period period;
    private final Skin skin;
    public Choice(Skin skin, ChoiceAction action, Period period){
        super(ActionFactory.ActionNames.CHOICE, skin);
        this.action = action;
        this.period = period;
        this.skin = skin;
        actors = new ArrayList<Actor>();
        Button back = new TextButton("Back", skin);
        choiceDialog = new Dialog("Available Choices", skin);
        choiceDialog.setWidth(500f);
        choiceDialog.setHeight(600f);
        choiceDialog.button(back);
        TextButton addChoice = new TextButton("Add Choices", skin);
        actors.add(addChoice);
        addChoice.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                choiceDialog.setPosition(getStage().getWidth() / 2, getStage().getHeight() / 2, Align.center);
                getStage().addActor(choiceDialog);
            }
        });
        back.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                choiceDialog.remove();
            }
        });
    }
    class ChoiceTextField extends TextField{
        private String oldName;
        public ChoiceTextField(String name, Skin skin){
            super(name, skin);
            oldName = name;
            setTextFieldListener(new TextFieldListener() {
                @Override
                public void keyTyped(TextField textField, char c) {
                    if (!action.choices.containsKey(getText())){
                        action.choices.put(getText(), action.choices.remove(oldName));
                        oldName = getText();
                    }
                }
            });
        }
    }
    @Override
    public List<Actor> getOptions() {
        refresh();
        return actors;
    }

    @Override
    public ChoiceAction getAction() {
        return action;
    }

    public void refresh(){
        Table contentTable = choiceDialog.getContentTable();
        contentTable.clear();
        contentTable.defaults().width(150f).pad(5);
        contentTable.setSkin(skin);
        contentTable.add("Choice Name");
        contentTable.add("Action to go to");
        contentTable.add("Remove");
        contentTable.row();
        final List<String> currentChoices = new ArrayList<String>(period.actions.size());
        for (Action a :period.actions){
            currentChoices.add(a.getText());
        }
        for (String choice: action.choices.keySet()){
            final ChoiceTextField choiceName = new ChoiceTextField(choice, skin);
            final SelectBox<String> choices = new SelectBox<String>(skin);
            choices.setItems(currentChoices.toArray(new String[currentChoices.size()]));
            choices.addListener(new ChangeListener() {
                @Override
                public void changed(ChangeEvent event, Actor actor) {
                    Choice.this.action.choices.put(choiceName.oldName, period.actions.get(choices.getSelectedIndex()));
                }
            });
            choices.setSelectedIndex(period.actions.indexOf(action.choices.get(choice)));
            TextButton remove = new TextButton("Remove", skin);
            remove.addListener(new ClickListener(){
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    super.clicked(event, x, y);
                    Choice.this.action.choices.remove(choiceName.oldName);
                    refresh();
                }
            });
            contentTable.add(choiceName);
            contentTable.add(choices);
            contentTable.add(remove);
            contentTable.row();
        }
        TextButton addNew = new TextButton("Add new choice", skin);
        addNew.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                String choiceName = "choice";
                int index = 1;
                while (action.choices.containsKey(choiceName)){
                    choiceName = "choice"+index++;
                }
                action.choices.put(choiceName, period.actions.get(0));
                refresh();
            }
        });
        contentTable.add(addNew).width(150).colspan(3).center().bottom();

    }

}
