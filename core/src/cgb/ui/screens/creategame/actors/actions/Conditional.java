package cgb.ui.screens.creategame.actors.actions;

import cgb.data.time.actions.ConditionalAction;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

import java.util.ArrayList;
import java.util.List;

public class Conditional extends ActionGui{
    private final ConditionalAction action;
    private final Dialog conditionalPicker;
    private final List<Actor> options;
    public Conditional(Skin skin, ConditionalAction action){
        super(ActionFactory.ActionNames.CONDITION, skin);
        this.action = action;
        conditionalPicker = new Dialog("Select Condition",skin);
        options = new ArrayList<>();
    }
    @Override
    public List<Actor> getOptions() {
        return null;
    }

    @Override
    public ConditionalAction getAction() {
        return action;
    }
}
