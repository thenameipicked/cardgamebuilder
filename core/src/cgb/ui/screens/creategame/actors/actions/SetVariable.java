package cgb.ui.screens.creategame.actors.actions;

import cgb.data.time.actions.SetVariableAction;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

import java.util.List;

public class SetVariable extends ActionGui{
    private final SetVariableAction action;
    public SetVariable(Skin skin, SetVariableAction action){
        super(ActionFactory.ActionNames.SET_VARIABLE, skin);
        this.action = action;
    }
    @Override
    public List<Actor> getOptions() {
        return null;
    }

    @Override
    public SetVariableAction getAction() {
        return action;
    }
}
