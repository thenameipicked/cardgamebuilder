package cgb.ui.screens.creategame.actors.actions.conditions;

import cgb.data.CardGame;
import cgb.data.time.Period;
import cgb.data.time.conditions.*;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

public class ConditionGuiFactory {
    public static ConditionGui createGui(Condition from, Skin skin, CardGame game, Period currentPeriod){
        if (from.getText().equals(ConditionFactory.ConditionNames.AND)){
            return new AndGui(skin, (AndCondition) from);
        } else if (from.getText().equals(ConditionFactory.ConditionNames.DECK_SIZE)){
            return new DeckSizeGui(skin, (DeckSizeCondition) from);
        } else if (from.getText().equals(ConditionFactory.ConditionNames.MATCHING_ATTRIBUTES)){
            return new MatchingAttributeGui(skin, (MatchingAttributeCondition)from);
        } else if (from.getText().equals(ConditionFactory.ConditionNames.NOT)){
            return new NotGui(skin, (NotCondition)from);
        } else if (from.getText().equals(ConditionFactory.ConditionNames.OR)){
            return new OrGui(skin,(OrCondition) from);
        } else if (from.getText().equals(ConditionFactory.ConditionNames.TRUE)){
            return new TrueGui(skin, (TrueCondition)from);
        }
        return new TrueGui(skin, new TrueCondition());
    }
}
