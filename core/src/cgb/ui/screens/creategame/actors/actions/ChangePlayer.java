package cgb.ui.screens.creategame.actors.actions;

import cgb.data.time.actions.ChangePlayerAction;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

import java.util.List;

public class ChangePlayer extends ActionGui{
    private final ChangePlayerAction action;
    public ChangePlayer(Skin skin, ChangePlayerAction action){
        super(ActionFactory.ActionNames.CHANGE_PLAYER, skin);
        this.action = action;
    }
    @Override
    public List<Actor> getOptions() {
        return null;
    }

    @Override
    public ChangePlayerAction getAction() {
        return action;
    }
}
