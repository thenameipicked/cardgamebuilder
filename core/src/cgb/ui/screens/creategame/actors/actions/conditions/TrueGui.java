package cgb.ui.screens.creategame.actors.actions.conditions;

import cgb.data.time.conditions.TrueCondition;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

public class TrueGui extends ConditionGui{
    TrueCondition condition;
    public TrueGui(Skin skin, TrueCondition condition){
        super(skin);
        this.condition = condition;
    }

    @Override
    public TrueCondition getCondition() {
        return condition;
    }
}
