package cgb.ui.screens.creategame.actors.actions;

import cgb.data.time.actions.ShuffleAction;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

import java.util.List;

public class Shuffle extends ActionGui{
    private final ShuffleAction action;
    public Shuffle(Skin skin, ShuffleAction action){
        super(ActionFactory.ActionNames.SHUFFLE, skin);
        this.action = action;
    }
    @Override
    public List<Actor> getOptions() {
        return null;
    }

    @Override
    public ShuffleAction getAction() {
        return action;
    }
}
