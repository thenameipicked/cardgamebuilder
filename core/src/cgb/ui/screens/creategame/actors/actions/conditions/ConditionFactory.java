package cgb.ui.screens.creategame.actors.actions.conditions;

import cgb.data.CardGame;
import cgb.data.time.conditions.*;

import java.util.ArrayList;
import java.util.List;

public class ConditionFactory {
    public static final class ConditionNames{
        public static final String DECK_SIZE = "Deck has size";
        public static final String MATCHING_ATTRIBUTES = "Attributes match";
        public static final String TRUE = "Always";
        public static final String AND = "And";
        public static final String OR = "Or";
        public static final String NOT = "Not";
        public static final List<String> allConditionNames = new ArrayList<String>();
        static {
            allConditionNames.add(DECK_SIZE);
            allConditionNames.add(MATCHING_ATTRIBUTES);
            allConditionNames.add(TRUE);
            allConditionNames.add(AND);
            allConditionNames.add(OR);
            allConditionNames.add(NOT);
        }
    }
    public static Condition createCondition(String actionName, CardGame game){
        if (actionName.equals(ConditionNames.DECK_SIZE)){
            return new DeckSizeCondition();
        } else if (actionName.equals(ConditionNames.MATCHING_ATTRIBUTES)){
            return new MatchingAttributeCondition();
        } else if (actionName.equals(ConditionNames.TRUE)){
            return new TrueCondition();
        } else if (actionName.equals(ConditionNames.AND)){
            return new AndCondition();
        } else if (actionName.equals(ConditionNames.OR)){
            return new OrCondition();
        } else if (actionName.equals(ConditionNames.NOT)){
            return new NotCondition();
        }
        return null;
    }
}
