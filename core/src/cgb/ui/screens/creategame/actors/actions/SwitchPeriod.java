package cgb.ui.screens.creategame.actors.actions;

import cgb.data.time.actions.PeriodAction;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

import java.util.List;

public class SwitchPeriod extends ActionGui{
    private final PeriodAction action;
    public SwitchPeriod(Skin skin, PeriodAction action){
        super(ActionFactory.ActionNames.PERIOD, skin);
        this.action = action;
    }
    @Override
    public List<Actor> getOptions() {
        return null;
    }

    @Override
    public PeriodAction getAction() {
        return action;
    }
}
