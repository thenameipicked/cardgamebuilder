package cgb.ui.screens.creategame.actors.actions;

import cgb.data.card.CardTable;
import cgb.data.time.actions.MoveCardAction;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class MoveCard extends ActionGui{
    private final MoveCardAction action;
    private final Label deckToLabel;
    private final SelectBox<String> deckTo;
    private final SelectBox<MoveCardAction.MoveOptions> toPosition;
    private final Label deckFromLabel;
    private final SelectBox<String> deckFrom;
    private final SelectBox<MoveCardAction.MoveOptions> fromPosition;
    private final ArrayList<Actor> optionsList;


    public MoveCard(Skin skin, final MoveCardAction action, final CardTable table){
        super(ActionFactory.ActionNames.MOVE, skin);
        this.action = action;
        deckFromLabel = new Label("Deck to take from:", skin);
        deckFrom = new SelectBox<String>(skin);
        toPosition = new SelectBox<MoveCardAction.MoveOptions>(skin);
        deckToLabel = new Label("Deck to add to:", skin);
        deckTo = new SelectBox<String>(skin);
        fromPosition = new SelectBox<MoveCardAction.MoveOptions>(skin);

        Set<String> deckNames = table.getDeckNames();
        String[] arrayDeckNames =deckNames.toArray(new String[deckNames.size()]);
        deckFrom.setItems(arrayDeckNames);
        deckTo.setItems(arrayDeckNames);

        toPosition.setItems(MoveCardAction.MoveOptions.values());
        fromPosition.setItems(MoveCardAction.MoveOptions.values());

        optionsList = new ArrayList<>(6);
        optionsList.add(deckToLabel);
        optionsList.add(deckTo);
        optionsList.add(toPosition);
        optionsList.add(deckFromLabel);
        optionsList.add(deckFrom);
        optionsList.add(fromPosition);

        deckFrom.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                action.from = table.getDeck(deckFrom.getSelected());
            }
        });


        deckTo.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                action.to = table.getDeck(deckTo.getSelected());
            }
        });

        toPosition.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                action.toOption = toPosition.getSelected();
            }
        });


        fromPosition.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                action.fromOption = fromPosition.getSelected();
            }
        });

    }
    @Override
    public List<Actor> getOptions() {
        return optionsList;
    }

    @Override
    public MoveCardAction getAction() {
        return action;
    }
}
