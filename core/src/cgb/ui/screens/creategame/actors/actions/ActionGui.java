package cgb.ui.screens.creategame.actors.actions;

import cgb.data.time.actions.Action;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

import java.util.List;

public abstract class ActionGui extends Label {
    public ActionGui(String text, Skin skin){
        super(text, skin);
        deselect();
    }
    public void select(){
        setColor(Color.PINK);
    }


    public void deselect(){
        setColor(Color.WHITE);
    }
    public abstract List<Actor> getOptions();
    public abstract Action getAction();
}
