package cgb.ui.screens.creategame.actors.actions.conditions;

import cgb.data.time.conditions.AndCondition;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

public class AndGui extends ConditionGui {
    AndCondition condition;
    public AndGui(Skin skin, AndCondition andCondition){
        super(skin);
    }

    @Override
    public AndCondition getCondition() {
        return condition;
    }
}
