package cgb.ui.screens.creategame.actors.actions;

import cgb.data.time.actions.NoneAction;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

import java.util.List;

public class None extends ActionGui{
    private final NoneAction action;
    public None(Skin skin, NoneAction action){
        super(ActionFactory.ActionNames.NONE, skin);
        this.action = action;
    }
    @Override
    public List<Actor> getOptions() {
        return null;
    }

    @Override
    public NoneAction getAction() {
        return action;
    }
}
