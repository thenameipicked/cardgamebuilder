package cgb.ui.screens.creategame.actors.actions.conditions;

import cgb.data.time.conditions.DeckSizeCondition;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

public class DeckSizeGui extends ConditionGui{
    DeckSizeCondition condition;
    public DeckSizeGui(Skin skin, DeckSizeCondition condition){
        super(skin);
        this.condition = condition;
    }

    @Override
    public DeckSizeCondition getCondition() {
        return condition;
    }
}
