package cgb.ui.screens.creategame.actors;

import cgb.data.card.Card;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;


public class CardGui extends Table {

    Label title;
    Label description;
    boolean isSelected;
    Card card;
    ClickListener listener;

    public CardGui(Card card, Skin skin){

        title = new Label(card.getTitle(), skin);
        description = new Label(card.getDescription(), skin);
        description.setWrap(true);
        ToggleHighlight highlight = new ToggleHighlight();
        title.addListener(highlight);
        description.addListener(highlight);
        isSelected = false;
        this.card = card;
        description.setAlignment(Align.topLeft);

    }

    public void setClickListener(ClickListener listener){
        this.listener = listener;
    }

    public Card getCard() {
        return card;
    }
    public void update(){
        title.setText(card.getTitle());
        description.setText(card.getDescription());
    }

    public boolean isSelected() {
        return isSelected;
    }
    public void click(){
        isSelected = !isSelected;
        Color nextColor = isSelected?Color.PINK:Color.WHITE;
        CardGui.this.title.setColor(nextColor);
        CardGui.this.description.setColor(nextColor);

    }
    public void selected(){
        isSelected = false;
        click();
    }

    public void notSelected(){
        isSelected = true;
        click();
    }

    class ToggleHighlight extends ClickListener {
        @Override
        public void clicked(InputEvent event, float x, float y) {
            super.clicked(event, x, y);
            if (listener != null){
                listener.clicked(event, x, y);
            }
        }
    }


    @Override
    protected void sizeChanged() {
        this.clear();
        this.add(title).height(getHeight()/4f).width(getWidth());
        this.row();
        this.add(description).height(getHeight()*3/4f).width(getWidth());

        title.setFontScale(getWidth() / 140f);
        description.setFontScale(getWidth()/140f);
    }

}





