package cgb.ui.screens.creategame.actors;

import cgb.data.card.CardTable;
import cgb.data.card.Deck;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;


public class DeckGui extends Table {

    private final TextField title;
    private final Label space;
    private final Deck deck;
    private String name;
    private boolean isSelected;
    public DeckGui(Deck deck, Skin skin, final CardTable table){
        String name = deck.getAttributes().getAttribute(Deck.DECK_NAME_ATTRIBUTE).value;
        title = new TextField(name, skin);
        this.name = name;
        space = new Label("", skin);
        title.setTextFieldListener(new TextField.TextFieldListener() {
            @Override
            public void keyTyped(TextField textField, char c) {
                if (!table.deckExists(textField.getText())){
                    table.renameDeck(DeckGui.this.name, textField.getText());
                    DeckGui.this.name = textField.getText();
                }
            }
        });
        this.deck = deck;
        isSelected = false;
        sizeChanged();
    }

    public void clicked(){
        isSelected = !isSelected;
        Color color = isSelected?Color.PINK:Color.WHITE;
        space.setColor(color);
        title.setColor(color);
    }

    @Override
    public boolean addListener(EventListener listener) {
        return space.addListener(listener) && title.addListener(listener);
    }

    public Deck getDeck() {
        return deck;
    }

    @Override
    protected void sizeChanged() {
        super.sizeChanged();
        this.clear();
        this.add(space).height(getHeight()*3/4).width(getWidth());
        this.row();
        this.add(title).height(getHeight()/4f).width(getWidth());
        this.row();
    }

}


