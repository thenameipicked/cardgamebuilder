package cgb.ui.screens.playmenu;

import cgb.data.CardGame;
import cgb.ui.screens.MenuScreen;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.FocusListener;

public class PlayGameScreen extends MenuScreen {


    public PlayGameScreen(final Game parent, Skin skin, final Screen origin, CardGame game) {
        super(parent, skin);
        final Button hostGame = new TextButton("Host Game", skin);
        final Button connect = new TextButton("Connect...", skin);

        final TextField ipField = new TextField("IP Address", skin);
        final TextField portField = new TextField("Port", skin);
        Button back = new TextButton("Main Menu", skin);

        addActor(hostGame);
        addActor(ipField);
        addActor(portField);
        connect.setVisible(false);
        addActor(connect);
        addActor(back, origin);
        ipField.addListener(new FocusListener() {
            @Override
            public boolean handle(Event event) {
                setConnectVisible(ipField.getText(), portField.getText(), connect);
                return super.handle(event);
            }
        });
        portField.addListener(new FocusListener() {
            @Override
            public boolean handle(Event event) {
                setConnectVisible(ipField.getText(), portField.getText(), connect);
                return super.handle(event);
            }
        });


    }

    private void setConnectVisible(String ip, String port, Button connect) {
        String ipRegEx = "(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9])\\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9])";
        String portRegEx = "\\d|[1-9]\\d|[1-9]\\d\\d|[1-9]\\d\\d\\d|[1-6]\\d\\d\\d\\d";
        if (ip.matches(ipRegEx) && port.matches(portRegEx))
            connect.setVisible(true);
        else
            connect.setVisible(false);


    }
}


