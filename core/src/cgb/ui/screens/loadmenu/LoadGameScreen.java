package cgb.ui.screens.loadmenu;

import cgb.Main;
import cgb.data.CardGame;
import cgb.io.SaveGame;
import cgb.ui.screens.MenuScreen;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

public class LoadGameScreen extends MenuScreen {


        public LoadGameScreen(final Main parent, Skin skin, final Screen origin, CardGame game) {
            super(parent, skin);

            final TextButton loadGame = new TextButton("Load Selected", skin);
            final TextButton deleteGame = new TextButton("Delete Selected", skin);
            final SelectBox<String> list = new SelectBox<String>(skin);
            list.setItems(SaveGame.getGameNames());
            if (list.getItems().size > 0) {
                list.setSelectedIndex(0);
            } else {
                loadGame.setDisabled(true);
                loadGame.setColor(Color.DARK_GRAY);
            }

            loadGame.addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    super.clicked(event, x, y);
                    parent.refreshGame(SaveGame.getGame(list.getSelected()));
                }
            });
            deleteGame.addListener(new ClickListener(){
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    super.clicked(event, x, y);
                    SaveGame.getGame(list.getSelected());
                }
            });
            addActor(list);
            addActor(loadGame);
            addActor(new TextButton("Back", skin), origin);
        }

}
