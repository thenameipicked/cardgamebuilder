package cgb.ui.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import java.util.ArrayList;
import java.util.List;

/**
 * The default menu screen.  Automatically positions the buttons to be in the center equally spaced apart
 */
public class MenuScreen extends BaseScreen{
    private final Table table;
    private final List<Actor> actors;

    public MenuScreen(Game parent, Skin skin){
        super(parent);
        actors = new ArrayList<Actor>();
        table = new Table(skin);
        stage.addActor(table);
    }
    public void addActor(final Actor actor, final Screen switchToScreen){
        addActor(actor);

        actor.addListener(new ClickListener() {

            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                parent.setScreen(switchToScreen);
            }
        });
    }
    public void addActor(final Actor actor){
        actors.add(actor);
        setButtonPositions();

    }
    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
        table.setWidth(width);
        table.setHeight(height);
        setButtonPositions();
    }

    @Override
    public void show() {
        super.show();
        setButtonPositions();
        Gdx.input.setInputProcessor(stage);
    }

    private void setButtonPositions(){
        table.clear();

        for(Actor actor: actors){
            table.add(actor).pad(10);
            table.row();


        }

    }

}
