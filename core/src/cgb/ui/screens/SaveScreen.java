package cgb.ui.screens;

import cgb.data.CardGame;
import cgb.io.SaveGame;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

public class SaveScreen extends MenuScreen{
    TextButton save;
    TextField fileName;


    public SaveScreen(final Game parent, Skin skin, final Screen origin, final CardGame game) {
        super(parent, skin);
        save = new TextButton("Save", skin);
        Button back = new TextButton("Main Menu", skin);
        String name = game.hasFileName()?game.getFileName():"game";
        fileName = new TextField(name, skin);
        addActor(fileName);
        addActor(save);
        addActor(back, origin);

        save.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                SaveGame.saveGame(game, fileName.getText());
                parent.setScreen(origin);
            }
        });
    }
}
