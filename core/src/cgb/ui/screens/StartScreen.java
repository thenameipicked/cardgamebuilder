package cgb.ui.screens;


import cgb.data.CardGame;
import cgb.ui.screens.creategame.CreateGameScreen;
import cgb.ui.screens.loadmenu.LoadGameScreen;
import cgb.ui.screens.playmenu.PlayGameScreen;
import cgb.Main;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;

public class StartScreen extends MenuScreen{
    CreateGameScreen createScreen;
    LoadGameScreen loadScreen;
    PlayGameScreen playScreen;
    SaveScreen saveScreen;

    public StartScreen(Main origin, Skin skin, CardGame game){
        super(origin, skin);

        createScreen = new CreateGameScreen(origin, skin, this, game);
        loadScreen = new LoadGameScreen(origin, skin, this, game);
        playScreen = new PlayGameScreen(origin, skin, this, game);
        saveScreen = new SaveScreen(origin, skin, this, game);

        addActor(new TextButton("Create New Game", skin), createScreen);
        addActor(new TextButton("Save Game", skin), saveScreen);
        addActor(new TextButton("Load Game", skin), loadScreen);
        addActor(new TextButton("Play Game", skin), playScreen);
    }

}
